import mne
import numpy as np

mne.set_log_level('WARNING')

raw = mne.io.read_raw_brainvision("raw/abra_0001.vhdr",
                                  eog=("EOGre", "EOGli","EOGunre","EOGobre"),
                                  misc=None,
                                  preload=True)
picks = mne.pick_types(raw.info, eeg=True, eog=True, stim=False, misc=False)
raw.filter(0.15, None, l_trans_bandwidth=0.1)
raw = mne.io.set_eeg_reference(raw)[0]
events = raw.get_brainvision_events()

# event trigger and conditions
event_id = {'iien': 182,
            'iiev': 181, 
            'iaen': 172,
            'iaev': 171,
            'aien': 162,
            'aiev': 161, 
            'aaen': 152,
            'aaev': 151,
            'iipn': 142,
            'iipv': 141,
            'iapn': 132,
            'iapv': 131,
            'aipn': 122,
            'aipv': 121,
            'aapn': 112,
            'aapv': 111} 

# extract events that had a correct response
eventsq = events[:,2]
useevents = np.zeros(len(eventsq),dtype=bool)
items = np.zeros(len(eventsq),dtype=int)
item = None

for i,e in enumerate(eventsq):
    if 110 < e < 190:
        if e % 10 == 1:
            if eventsq[i+4] == 196:
                useevents[i] = True;    
                items[i] = item;    
        elif e % 10 == 2:
            if eventsq[i+3] == 196:
                useevents[i] = True;
                items[i] = item;            
        else:
            pass
    elif e > 200:
        item = e
    else:
        pass

events[:,1] = items
events = events[useevents]

tmin = 0.2
tmax = 1.2
baseline = None
reject = dict(eeg=40e-6, eog=250e-6)
epochs = mne.Epochs(raw, events,
                    event_id,
                    tmin, tmax,
                    baseline=baseline,
                    preload=False,
                    reject=None,
                    picks=picks,
                    on_missing='warning')
evoked = {}
for e in event_id:
    evoked[e] = epochs[e].average()

#e.pick_channels(["Cz"]).plot()

