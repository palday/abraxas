import pyqrcode
import os.path
url = pyqrcode.create('http://dx.doi.org/10.6084/m9.figshare.3507347')
url.eps(os.path.join('figures','poster-snl2016-url.eps'), scale=2) 