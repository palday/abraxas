# Abraxas German ERP

## Test subjects

08: incorrect trigger codes in original VMRK file, correct post-hoc
17: exclude first 10 trials, they were presented twice and only the second time (i.e. the time we can't use) was recorded.
31: lots of eye movements

# Abraxas German eyes
Anbei ist eine kurze Liste, welche Trials aus den Inputdateien noch nachträglich für die Analyse gelöscht werden müssen. Alle Fixationszeiten mit 0 müssen übrigens auch rausgenommen werden. Pro VP und Bedingung bleiben aber noch genügend Trials übrig (wenigstens 4 von insgesamt 6).
Es fallen auch keine VPs raus.

#exclude trials with blinks in NP regions (changed 05 2016; original data screened for blinks on verbs only)

data$cond[data$subj == "ABU09" & data$cond == 12 & data$item == 7] <- 0
data$cond[data$subj == "ABU09" & data$cond == 12 & data$item == 12] <- 0
data$cond[data$subj == "ABU09" & data$cond == 11 & data$item == 6] <- 0
data$cond[data$subj == "ABU41" & data$cond == 1 & data$item == 27] <- 0
data$cond[data$subj == "ABU47" & data$cond == 7 & data$item == 29] <- 0
data$cond[data$subj == "ABU78" & data$cond == 6 & data$item == 25] <- 0

#exclude trials with long fixation on NP (changed 05 2016; in original data, trials only deletend with long fixations on verbs)
data$cond[data$subj == "ABU20" & data$cond == 8 & data$item == 15] <- 0
data$cond[data$subj == "ABU66" & data$cond == 1 & data$item == 34] <- 0
data$cond[data$subj == "ABU77" & data$cond == 3 & data$item == 3] <- 0
data$cond[data$subj == "ABU33" & data$cond == 7 & data$item == 16] <- 0
data$cond[data$subj == "ABU41" & data$cond == 1 & data$item == 30] <- 0
data$cond[data$subj == "ABU78" & data$cond == 1 & data$item == 29] <- 0
data$cond[data$subj == "ABU46" & data$cond == 7 & data$item == 19] <- 0

data1 <- data[data$cond != 0,]
