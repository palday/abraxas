---
title: 'On the relationship between eye movements and the N400 in sentence processing:
  A unifying statistical approach'
author:
- Phillip M. Alday
- Franziska Kretzschmar
output: word_document
---

There is converging evidence that domain-general principles such as predictive coding provide better explanations for sentence processing than linguistic accounts.
This view bears implications for the correspondence between event-related potentials (ERPs), especially the N400, and eye movements in sentence reading.
Instead of linking N400 and fixation measures via some linguistic subdomain, the domain-general approach postulates prediction error as the common basis of N400 and eye movements (Bornkessel-Schlesewsky et al. 2016; Friston et al. 2012).
This predicts that both measures correlate only if they follow the same prediction error, without targeting a particular eye movement measure.
To test this hypothesis we re-analysed data from two eye-movement studies (N=116) and one ERP study (N=37) that investigated animacy-based prediction errors of actor prototypicality with identical stimuli.
The experiments replicated previous results by revealing larger N400 amplitudes and longer go-past time for unpredicted atypical actors.
To assess whether reading time and N400 correlate, we aggregated one measure across participants to provide a numeric predictor for the other with mixed-effects models.
Notably, the two participant groups do not overlap.
For modelling the ERP data, aggregate go-past time provided a better model fit than aggregate first-past time, although the "design" model based on the experimental manipulation still provided the best fit.
For modelling the eye-tracking data, both design and aggregate N400 responses were better able to model go-past time than first-past time (i.e. overall better model fit). 
In both cases, the design model provided a slightly better fit.
The difference in dimensionality remains a fundamental constraint when comparing these predictors, e.g. one-dimensional eye-movement measures are not able to capture complex interactions in a linear model.
This method nonethless quantifies the relationship between eye-movement measures and electrophysiology and thereby supports the view that N400 and reading time correlate in predictive processing when resulting from similar prediction error.



