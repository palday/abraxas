# -*- coding: utf-8 -*-

import pylab
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import mne
import numpy as np
import os.path
import seaborn as sns
from mne.viz import plot_evoked_topo

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay",scale=True)
layout.pos[:,2] = layout.pos[:,2] * 4
layout.pos[:,3] = layout.pos[:,3] * 5
montage = mne.channels.read_montage(kind="standard_1020")

try:
    import cPickle as pickle
except ImportError:
    import pickle

with open("cache.pickle","rb") as pfile:
    evoked = pickle.load(pfile)
    evoked_by_cond = pickle.load(pfile)
    grand_average = pickle.load(pfile)

# event trigger and conditions
event_id = {'iien': 182,
            'iiev': 181,
            'iaen': 172,
            'iaev': 171,
            'aien': 162,
            'aiev': 161,
            'aaen': 152,
            'aaev': 151,
            'iipn': 142,
            'iipv': 141,
            'iapn': 132,
            'iapv': 131,
            'aipn': 122,
            'aipv': 121,
            'aapn': 112,
            'aapv': 111}

blue = (0.25646972,  0.48715914,  0.6570037)
red  = (0.85453713,  0.22957019,  0.2762321)
sns.set_style("whitegrid")

# plotting options
color_dict = {'anim':red, 'inanim':blue}
line_dict = {'act':'-','pass':':'}

axis_width = 0.75

min_trials = 60
for e in evoked.keys():
    if sum(evoked[e][c].nave for c in evoked[e]) < min_trials:
        del evoked[e]

print ("{} subjects with at least {} ".format(len(evoked.keys()), min_trials) +
       "trials included in grand average plots")

evoked_by_cond = {e:[evoked[s][e] for s in evoked] for e in event_id}
grand_average = {cond:mne.grand_average(evoked_by_cond[cond]) for cond in evoked_by_cond}

grid_chans = ['F3','Fz','F4','C3','Cz','C4','P3','Pz','P4']
grid_picks = mne.pick_channels(grand_average['iien'].ch_names,grid_chans) 

# convert compact conditions to MNE-style hierarchical delimiters
def expand(s):
    s = s.replace("n","/noun")
    s = s.replace("v","/verb")
    
    s = s.replace("e","/es")
    s = s.replace("p","/pp")
    
    # MNE delims aren't purely hierarchical, so we name verb animacy bias "act"/"pass"
    # as a proxy for controlled/driven
    s = s.replace("aa","act/anim")
    s = s.replace("ai","act/inanim")
    s = s.replace("ia","pass/anim")
    s = s.replace("ii","pass/inanim")
    
    return s
 
#ssavj_hierarch = {expand(e):[evoked[s][e].copy().resample(100) for s in evoked] for e in event_id  if 'n' in e}  

#ssavj_pp = {e:ssavj_hierarch[e] for e in ssavj_hierarch if "pp" in e}
#ssavj_es = {e:ssavj_hierarch[e] for e in ssavj_hierarch if "es" in e}

#mne.viz.plot_compare_evokeds(ssavj_pp, picks=grid_picks,colors=color_dict,linestyles=line_dict,ci=None,invert_y=True,gfp=False)

time = np.linspace(-200,1200,num=701)

layout = mne.channels.read_layout("EEG1005.lay",scale=True)
#x,y,width,height,
layout.pos[:,0] = layout.pos[:,0] * 1.2 - 0.25 * np.mean(layout.pos[:,0])
layout.pos[:,1] = layout.pos[:,1] *  1.5 - 0.5 * np.mean(layout.pos[:,1])
layout.pos[:,2] = layout.pos[:,2] * 5
layout.pos[:,3] = layout.pos[:,3] * 6

def display_filter(evoked,l_freq=None,h_freq=8,phase='zero',filter_length=349,h_trans_bandwidth=6,n_jobs=2,fir_window='hamming',method='fir'):
    evoked = evoked.copy()
    evoked.data = mne.filter.filter_data(evoked.data, evoked.info['sfreq'], 
                                         l_freq=l_freq, h_freq=h_freq, 
                                         phase=phase, n_jobs=n_jobs,
                                         h_trans_bandwidth=h_trans_bandwidth,
                                         filter_length=filter_length,
                                         fir_window=fir_window, method=method, copy=True)
        
    return evoked

for pos in ('n','v'):
    for pf in ('e','p'):
        aa = grand_average['aa'+pf+pos].copy()
        ai = grand_average['ai'+pf+pos].copy()
        ia = grand_average['ia'+pf+pos].copy()
        ii = grand_average['ii'+pf+pos].copy()

        aa = display_filter(aa)
        ia = display_filter(ia)
        ai = display_filter(ai)
        ii = display_filter(ii)
        
        # topographic plot
        for ax, idx in mne.viz.iter_topography(aa.info,layout=layout,
                       fig_facecolor='white',
                       axis_facecolor='white',
                       axis_spinecolor='black',
                       on_pick=None):

            if aa.ch_names[idx] == "Ref":
                pass
                #h, l = plt.gca().get_legend_handles_labels()
                #ax.legend(h, l)
            else:
                if aa.ch_names[idx][-1] in [str(_) for _ in 3,5,7]:
                    ax.set_anchor('S')
                elif aa.ch_names[idx][-1] in [str(_) for _ in 4,6,8]:
                    ax.set_anchor('N')

                ax.plot(time,aa.data[idx,:]*1e6,color=blue,label="anim verb - anim noun",linewidth=0.5)
                ax.plot(time,ai.data[idx,:]*1e6,color=red,label="anim verb - inanim noun",linewidth=0.5)
                ax.plot(time,ia.data[idx,:]*1e6,':',color=red, label="inanim verb - anim noun",linewidth=0.5)
                ax.plot(time,ii.data[idx,:]*1e6,':',color=blue, label="inanim -verb - inanim noun",linewidth=0.5)
                ax.set_xlim(time[0],time[-1])
                ax.hlines(0,time[0],time[-1],linewidth=axis_width)
                ylim = 4,-4
                ax.set_ylim(*ylim)
                ax.set_yticks(ylim)
                ax.set_yticklabels([str(_) +'   ' for _ in ylim],ha='right',x=0.3)
                ax.vlines([0,250,500,750,1000],ymin=-0.5,ymax=0.5,linewidth=axis_width)
                ax.vlines(0,*ylim,linewidth=axis_width)
                ax.set_title(aa.ch_names[idx])

        plt.gcf().suptitle("ERPs at the {} with {} in the prefield".format('noun' if pos == 'n' else 'verb',
                                                                           'es' if pf == 'e' else 'PP'),fontsize=16)
        plt.gcf().set_size_inches(11.69,8.27) # A4 paper
        h, l = plt.gca().get_legend_handles_labels()
        plt.gcf().legend(h,l,loc=(0.0,0.0))
        sns.despine(top=True, right=True, left=True, bottom=True)
        plt.savefig(os.path.join("figures","topo_erp_{}_{}_prefield.pdf".format('noun' if pos == 'n' else 'verb',
                                                                                'es' if pf == 'e' else 'PP')))
        #plt.show(block=False)
        plt.close()
        
        # grid plot
        for i, ch_name in enumerate(grid_chans,start=1):
            aa_chan = aa.copy().pick_channels([ch_name])
            ai_chan = ai.copy().pick_channels([ch_name])
            ia_chan = ia.copy().pick_channels([ch_name])
            ii_chan = ii.copy().pick_channels([ch_name])      
            
            plt.subplot(4,3,i)
            plt.plot(time,aa_chan.data[0,:]*1e6,
                    linewidth=0.75, color=blue, label="animate verb - animate noun")
            plt.plot(time,ai_chan.data[0,:]*1e6,
                    linewidth=0.75, color=red, label="animate verb - inanimate noun")
            plt.plot(time,ia_chan.data[0,:]*1e6,
                    linewidth=0.85, color=blue, label="inanimate verb - animate noun")[0].set_dashes([1,1])
            plt.plot(time,ii_chan.data[0,:]*1e6,
                    linewidth=0.85, color=red, label="inanimate verb - inanimate noun")[0].set_dashes([1,1])
            plt.grid(linewidth=0.5)
            plt.text(0.90*time[-1], -3.8, ch_name,
                    horizontalalignment='center', verticalalignment='center')


            plt.xlim(time[0],time[-1])            
            plt.hlines(0,time[0],time[-1],linewidth=axis_width)
            if i == 8:
                plt.xlabel(u"Time (ms)")
                plt.xticks([0,250,500,750,1000])
                plt.gca().tick_params(axis='x', labelsize=9)
            else:
                plt.xticks([0,250,500,750,1000],[]*5)

            ylim = 5.3,-5.0
            plt.ylim(*ylim)
            plt.vlines(0,*ylim,linewidth=axis_width)
            if i == 4:
                plt.ylabel(u"µV")
                plt.yticks([4,2,0,-2,-4],[4,"",0,"",-4])
                plt.gca().tick_params(axis='y', labelsize=9)
            else:
                plt.yticks([4,2,0,-2,4],[]*5)
                
        plt.gcf().suptitle("ERPs at the {} with {} in the prefield".format(
            'noun' if pos == 'n' else 'verb',
            'es' if pf == 'e' else 'PP'))
        
        plt.gcf().set_size_inches(6.8,5)
        plt.legend(ncol=2,loc = 'lower center', bbox_to_anchor = (0.5,0.1),
                    bbox_transform = plt.gcf().transFigure)
        sns.despine(top=True, right=True, left=True, bottom=True)
        plt.savefig(os.path.join("figures","grid_erp_{}_{}_prefield.pdf".format(
            'noun' if pos == 'n' else 'verb',
            'es' if pf == 'e' else 'PP')), bbox_inches='tight')
        #plt.show()
        plt.close()

        # Cz plot
        aa = aa.pick_channels(['Cz'])
        ai = ai.pick_channels(['Cz'])
        ia = ia.pick_channels(['Cz'])
        ii = ii.pick_channels(['Cz'])

        plt.plot(time,aa.data[0,:]*1e6,color=blue,label="anim verb - anim noun")
        plt.plot(time,ai.data[0,:]*1e6,color=red,label="anim verb - inanim noun")
        plt.plot(time,ia.data[0,:]*1e6,':',color=blue, label="inanim verb - anim noun")
        plt.plot(time,ii.data[0,:]*1e6,':',color=red, label="inanim verb - inanim noun")

        plt.xlim(time[0],time[-1])
        plt.hlines(0,time[0],time[-1])
        plt.xlabel(u"Time (ms)")

        ylim = 5,-5
        plt.ylim(*ylim)
        plt.vlines(0,*ylim)
        plt.ylabel(u"µV")

        plt.title("Electrode Cz at the {} with {} in the prefield".format(
            'noun' if pos == 'n' else 'verb',
            'es' if pf == 'e' else 'PP'))
        plt.legend()
        sns.despine(top=True, right=True, left=True, bottom=True)
        plt.savefig(os.path.join("figures","erp_{}_{}_prefield.pdf".format(
            'noun' if pos == 'n' else 'verb',
            'es' if pf == 'e' else 'PP')), bbox_inches='tight')
        #plt.show()
        plt.close()

    # topographic plots averaged over prefields
    aa = mne.grand_average( [grand_average['aa'+pf+pos].copy() for pf in ('e','p')] )
    ai = mne.grand_average( [grand_average['ai'+pf+pos].copy() for pf in ('e','p')] )
    ia = mne.grand_average( [grand_average['ia'+pf+pos].copy() for pf in ('e','p')] )
    ii = mne.grand_average( [grand_average['ii'+pf+pos].copy() for pf in ('e','p')] )

    aa = display_filter(aa)
    ia = display_filter(ia)
    ai = display_filter(ai)
    ii = display_filter(ii)

    for ax, idx in mne.viz.iter_topography(aa.info,layout=layout,
                   fig_facecolor='white',
                   axis_facecolor='white',
                   axis_spinecolor='black',
                   on_pick=None):

        if aa.ch_names[idx] == "Ref":
            pass
            #h, l = plt.gca().get_legend_handles_labels()
            #ax.legend(h, l)
        else:
            if aa.ch_names[idx][-1] in [str(_) for _ in 3,5,7]:
                ax.set_anchor('S')
            elif aa.ch_names[idx][-1] in [str(_) for _ in 4,6,8]:
                ax.set_anchor('N')

            ax.plot(time,aa.data[idx,:]*1e6,color=blue,
                    label="anim verb - anim noun",linewidth=0.5)
            ax.plot(time,ai.data[idx,:]*1e6,color=red,
                    label="anim verb - inanim noun",linewidth=0.5)
            ax.plot(time,ia.data[idx,:]*1e6,':',color=blue,
                    label="inanim verb - anim noun",linewidth=0.5)
            ax.plot(time,ii.data[idx,:]*1e6,':',color=red,
                    label="inanim -verb - inanim noun",linewidth=0.5)

            ax.set_xlim(time[0],time[-1])
            ax.hlines(0,time[0],time[-1],linewidth=axis_width)
            ylim = 4,-4
            ax.set_ylim(*ylim)
            ax.set_yticks(ylim)
            ax.set_yticklabels([str(_) +'   ' for _ in ylim],ha='right',x=0.3)
            ax.vlines([0,250,500,750,1000],ymin=-0.5,ymax=0.5,linewidth=axis_width)
            ax.vlines(0,*ylim,linewidth=axis_width)
            ax.set_title(aa.ch_names[idx])

    plt.gcf().suptitle(
        "ERPs at the {}, averaged over prefields".format(
            'noun' if pos == 'n' else 'verb',
            'es' if pf == 'e' else 'PP'),
        fontsize=16)
    plt.gcf().set_size_inches(11.69,8.27) # A4 paper
    h, l = plt.gca().get_legend_handles_labels()
    plt.gcf().legend(h,l,loc=(0.0,0.0))
    sns.despine(top=True, right=True, left=True, bottom=True)
    plt.savefig(os.path.join("figures","topo_{}.pdf".format('noun' if pos == 'n' else 'verb',)))
    #plt.show(block=False)
    plt.close()

    # Cz plot averaged over prefields
    aa = aa.pick_channels(['Cz'])
    ai = ai.pick_channels(['Cz'])
    ia = ia.pick_channels(['Cz'])
    ii = ii.pick_channels(['Cz'])

    plt.plot(time,aa.data[0,:]*1e6,color=blue,
             label="anim verb - anim noun")
    plt.plot(time,ai.data[0,:]*1e6,color=red,
             label="anim verb - inanim noun")
    plt.plot(time,ia.data[0,:]*1e6,':',color=blue,
             label="inanim verb - anim noun")
    plt.plot(time,ii.data[0,:]*1e6,':',color=red,
             label="inanim -verb - inanim noun")

    plt.xlim(time[0],time[-1])
    plt.hlines(0,time[0],time[-1])
    plt.xlabel(u"Time (ms)")

    ylim = 5,-5
    plt.ylim(*ylim)
    plt.vlines(0,*ylim)
    plt.ylabel(u"µV")

    plt.title(
        "Electrode Cz at the {}, averaged over prefields".format('noun' if pos == 'n' else 'verb'))
    plt.legend()
    sns.despine(top=True, right=True, left=True, bottom=True)
    plt.savefig(
        os.path.join("figures","erp_{}.pdf".format('noun' if pos == 'n' else 'verb')),
        bbox_inches='tight')
    #plt.show()
    plt.close()