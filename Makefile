# Copyright (c) 2016, Phillip Alday
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
logos = figures/logo_unisa_RGB-blue.png figures/cnl-color.eps figures/logo_mainz_new.eps
tables = eyes-across-languages.tex eyes-from-eeg.tex eeg-from-eyes.tex eeg-from-eyes-model.tex eyes-from-eeg-model.tex
figures = figures/gopast-de.pdf figures/gopast-en.pdf figures/erp_animate_verb.pdf figures/erp_inanimate_verb.pdf

stats_data = data/abra_0001.csv data/abra_0002.csv data/abra_0003.csv data/abra_0004.csv data/abra_0005.csv data/abra_0006.csv data/abra_0007.csv data/abra_0008.csv data/abra_0010.csv data/abra_0011.csv data/abra_0012.csv data/abra_0013.csv data/abra_0014.csv data/abra_0015.csv data/abra_0016.csv data/abra_0017.csv data/abra_0018.csv data/abra_0019.csv data/abra_0020.csv data/abra_0021.csv data/abra_0022.csv data/abra_0023.csv data/abra_0024.csv data/abra_0025.csv data/abra_0026.csv data/abra_0027.csv data/abra_0028.csv data/abra_0029.csv data/abra_0030.csv data/abra_0031.csv data/abra_0032.csv data/abra_0033.csv data/abra_0034.csv data/abra_0035.csv data/abra_0036.csv data/abra_0037.csv 

epoch_data = data/abra_0001-epo.fif.gz data/abra_0002-epo.fif.gz data/abra_0003-epo.fif.gz data/abra_0004-epo.fif.gz data/abra_0005-epo.fif.gz data/abra_0006-epo.fif.gz data/abra_0007-epo.fif.gz data/abra_0010-epo.fif.gz data/abra_0011-epo.fif.gz data/abra_0012-epo.fif.gz data/abra_0013-epo.fif.gz data/abra_0014-epo.fif.gz data/abra_0015-epo.fif.gz data/abra_0016-epo.fif.gz data/abra_0017-epo.fif.gz data/abra_0018-epo.fif.gz data/abra_0019-epo.fif.gz data/abra_0020-epo.fif.gz data/abra_0021-epo.fif.gz data/abra_0022-epo.fif.gz data/abra_0023-epo.fif.gz data/abra_0024-epo.fif.gz data/abra_0025-epo.fif.gz data/abra_0026-epo.fif.gz data/abra_0027-epo.fif.gz data/abra_0028-epo.fif.gz data/abra_0029-epo.fif.gz data/abra_0030-epo.fif.gz data/abra_0031-epo.fif.gz data/abra_0032-epo.fif.gz data/abra_0033-epo.fif.gz data/abra_0034-epo.fif.gz data/abra_0035-epo.fif.gz data/abra_0036-epo.fif.gz data/abra_0037-epo.fif.gz

em_data_de = data/eyes_de/fp.txt data/eyes_de/gp.txt data/eyes_de/ro.txt data/eyes_de/tt.txt
em_data_en = data/eyes_en/ff.txt data/eyes_en/gp.txt data/eyes_en/ro.txt data/eyes_en/fp.txt data/eyes_en/ri.txt data/eyes_en/tt.txt

em_data := $(em_data_de) $(em_data_en)

.PHONY: snl-draft clean superclean

# keep make from cleaning up from itself and removing preprocessing output, etc.
.SECONDARY: 
.INTERMEDIATE: preproc snl_analysis

snl-draft: poster-snl2016.pdf 
	cp poster-snl2016.pdf poster-snl2016_`git show -s --format=%ci HEAD | awk '{print $$1}'`_`git rev-parse --short HEAD`.pdf

poster-snl2016.pdf: poster-snl2016.tex beamerthemeRJHlandscape.sty poster.bst figures/poster-snl2016-url.eps $(logos) $(tables) $(figures)
	latexmk -pdf -silent poster-snl2016.tex

%.pdf : %.md
	pandoc -o $@ $<

$(tables) figures/gopast-de.pdf figures/gopast-en.pdf : snl_analysis

snl_analysis : snl2016.R $(stats_data) $(em_data)
	R --vanilla --slave < $<

# use pattern matching so that make doesn't mistake this for two jobs
figures/erp_animate_verb.% figures/erp_inanimate_verb.%: poster_figures.py $(epoch_data)
	python $<

$(stats_data) $(epoch_data) : preproc

preproc: erp.py raw/*
	python $<

figures/poster-snl2016-url.eps: generateQRsnl2016.py
	python generateQRsnl2016.py
	
clean: 
	rm -f figures/*converted-to.pdf
	rm -f $(tables) $(figures)
	rm -f figures/poster-cns-url.eps
	rm -f Rplots.pdf
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.log *.nav *.snm *.toc *.synctex.gz

superclean: clean
	rm -f $(epoch_data) $(stats_data)
