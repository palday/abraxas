
##############################################################################
##                                                                          ##
##    Template : Visual ERP Study with Text Comprehension Questions         ##
##             Experiment Script for use with Presentation                  ##
##                 Adapted from Sylvia, Markus & Torsten                    ##
##                       Author: R. Muralikrishnan                          ##
##                                                                          ##
##############################################################################

##############################################################################
# Scenario Description Language (SDL) Header Part
##############################################################################

# These button codes have to correspond to those set in 'Settings -> Response' 
active_buttons = 2;      # There are 3 active buttons defined in 'Settings -> Response'
button_codes = 1,2;    # These are the codes of those buttons

response_matching = simple_matching;  # Enables features not in legacy_matching

# Only those button presses that one expects are logged; not all
response_logging = log_active;                

# Needed for EEG Trigger files
#write_codes = true;
#pulse_width = 5;
#default_output_port = 1;  # This is most probably going to be the same for everyone.
# This is the LPT Parallel port that you will define in the Port settings tab of 
# Presentation.  This will be used as the output port for sending codes to EEG.  
# Defining this here saves getting a port number in the PCL part from the output_manager
# and calling send_code etc.  Stimulus events themselves will send these for us, provided we
# define the correct port_codes in each Stimulus event.


# Default Settings for Font, FG and BG Colours etc.
default_background_color = "255, 255, 250"; # RGB codes in decimal; 000 => Black
default_font = "Verdana";
default_font_size = 32;
default_text_color = "0, 0, 0"; # #E8F0F7 An off-bluish white              
default_text_align = align_center;


default_deltat = 0;
default_picture_duration = next_picture; /* This implies that all pictures ...
/...are shown until the next picture is shown, unless otherwise specified */

#default_all_responses = false;


##############################################################################
# Scenario Description Language (SDL) Part
##############################################################################

begin;

# ============================
# SDL Variable Declarations: -
# ============================

# 
# We don't use any variables.  All the timer parameters appear as numbers.


# ==============================
# Picture Stimuli Definitions: - Everything that is shown, including text!
# ==============================


# Screen definition for the beginning
# Operator Controlled!
#picture { 
#	text {
#		caption = "Bitte warten...";
#	};
#	x = 0;
#	y = 0;
#} pic_before;


# Screen definition for the trial that starts an Experimental Session.
# Used as the first screen that the participant sees.  Participant Controlled.
picture 
{   
   text { caption = "Willkommen zu unserem Vortest!\n\nDeine Aufgabe ist es,\ndir einen kurzen Text durchzulesen.\n\nWenn du den Text fertig\ndurchgelesen hast,\ndrückst du bitte die linke Taste. \n\n\n~~~~~~\n\n\nBitte drücke nun die linke Taste,\num zu beginnen."; };
    x = 0;
    y = -20;
} P_Start_Exp;

# Screen definition for the Pause trial
# Operator Controlled!
picture 
{    
   bitmap { filename = "e2lsf.jpg"; };
    x = 0;
    y = 0;
} P_Text_Display;

# Screen definition for the Continue trial
# Participant Controlled!
picture 
{
    text { caption = " Kleine Pause! Bitte drücke die  linke Taste,\num das Experiment fortzusetzen."; };
    x = 0;
    y = 0;
} P_Continue;

# Screen definition for the Interruption trial
# Operator Controlled!
picture 
{    
   text { caption = "Ein Fehler ist aufgetreten.\nBitte warten."; };
    x = 0;
    y = 0;
} P_Interruption;

# Screen definition for showing the focus star
picture
{
   bitmap { filename = "fix_kreuz.bmp"; };     # Use Focus_Star.bmp
   x = -400;
   y = 400;
} P_Focus_Star;

# Screen definition for showing the question mark
#picture 
#{    
#   text { caption = "???"; };
#    x = 0;
#    y = 0;
#} P_Show_Question_Mark;

# Screen definition for the End_Thanks trial
picture 
{ 
    text {caption = "Das war der Vortest!";};
    x = 0;
    y = 0;
} P_End_Thanks;

####  Specific for presenting Visual Stimuli   --------------------------------
# Screen definition for the trial that presents Visual Stimuli word-by-word.
#picture 
#{   
#   text { caption = " "; } Txt_Visual_Stimulus;
#    x = 0;
#    y = 0;
#} P_Visual_Stimulus;

####

####  Specific for presenting Comprehension Questions from Textfile   ---------
# Screen definition for Comprehension questions (text)
#picture 
#{
#   text {caption = " ";} Txt_Comp_Question;    # PCL Program fills the caption.              
#   x = 0;
#   y = 0;
#} P_Comp_Question;

#####


#=====================
# Trial Definitions: -
#=====================

/**** 
/  Note that the name 'Trial' could be misleading.  Unless otherwise specified,
/  what we mean by a 'Trial' here is actually a sub-trial or sub-task that is
/  part - and thereby defines the structure - of an actual experimental trial.
****/
 
/****
/ Template for defining an SDL Trial. 
/ trial
/ {
/      Trial-related parameters such as trial_duration, trial_type etc.
/     Trial-related parameters
/
/     Stimulus event 1 such as picture, sound, video, nothing or force-FB;
/     Stimulus event parameters such as time, event code etc.
/     If the parameters aren't specified, the default values are used!!!
/     
/     Stimulus event 2;
/     its parameters...and so on
/ }
****/

# Screen definition of the Trial to show a Blank Screen between Experimental Trials
trial
{
    trial_duration = 1000;
    all_responses = false;
    
    stimulus_event
    {
        picture {};     # This is the default picture, which is nothing!
#        code = "CLS";   # Comment this out to avoid seeing this in the logfile.
    };
} T_Blank_Screen;



# Definition of the Trial before to start an Experimental Session.
# Operator Controlled!
#trial 
#{ 
 #   trial_duration = forever;       # Keep running the trial...    
  #  trial_type = specific_response; # ...until the following specific response.
   # terminator_button = 3;          # Operator PC - ENTER key.
   # stimulus_event 
   # {
  #      picture pic_before;        # Show P_Start_Exp.
        # code = "The Experiment starts now!";
  #  };
#} T_before;


# Definition of the Trial to start an Experimental Session.
# Participant Controlled!
trial 
{ 
    trial_duration = forever;       # Keep running the trial...    
    trial_type = specific_response; # ...until the following specific response.
    terminator_button = 1;          # Operator PC - ENTER key.
    
    stimulus_event 
    {
        picture P_Start_Exp;        # Show P_Start_Exp.
#        code = "Jetzt beginnt das Experiment!";
    };
} T_Start_Exp;


# Definition of the Trial to continue further.
# Participant Controlled!
trial 
{ 
    trial_duration = forever;        # Keep running the trial...    
    trial_type = specific_response;  # ...until the following specific response.
    terminator_button = 1;         # Participant Joystick - L or R button.
   
    stimulus_event
    {
        picture P_Continue;          # Show P_Continue.
#        code = "Trials follow!";
    };
} T_Continue;


# Definition of the Trial to Launch an Experimental Trial
trial
{
    trial_duration = 1000;       # Run the trial for 400 ms... 
    all_responses = false;      # ..without recognising any key presses.

    stimulus_event
    {
        picture P_Focus_Star;   # Show the focus star for fixation.
        code = "";              # Code set below in the PCL program.
        # port_code = ;         # Port code set by the PCL program.
    } E_Launch_New_Trial;
} T_Launch_New_Trial;


# Definition of the Trial to keep showing the Focus Star for fixation.
trial
{
    trial_duration = 1000;      # Show star until the next clear-screen... 
    all_responses = false;      # ..without recognising any key presses.
    stimulus_event
    {
        picture P_Focus_Star;   # Show the focus star for fixation.
        code = "*";             # Comment this out later to avoid seeing this in the log file.
    };
} T_Keep_Showing_Star;

# Definition of the Trial to keep showing the Question marks for fixation.
#trial
#{
#    trial_duration = 500;      # Show star until the next clear-screen... 
#    all_responses = false;      # ..without recognising any key presses.
#    stimulus_event
#    {
#        picture P_Show_Question_Mark;
#        code = "???";             # Comment this out later to avoid seeing this in the log file.
#    };
#} T_Show_Question_Mark;




####  Specific for presenting Visual Stimuli   --------------------------------
# Definition of the Trial to present Visual Stimuli word-by-word
trial
{
    trial_duration = forever;  # PCL Program changes this!!!
    trial_type = specific_response;  # ...until the following specific response.
    terminator_button = 1;
    
    stimulus_event
    {
        picture P_Text_Display;
        code = "155";  # PCL Program fills this!!!
         port_code = 155 ; # PCL Program fills this!!!
			target_button = 1;
    } E_Text_Display;
} T_Text_Display;

####

# Definition of the Trial to show the Comprehension Question
#trial
#{
#   trial_duration = 4000;       
#   trial_type = specific_response;
#   terminator_button = 1,2;  # The exact response can be either of these;
#    
#    stimulus_event
#    {
#        picture P_Comp_Question; 
#        code = "";                    # PCL Program sets this.
#        #port_code = 195;                   # Send '195' to EEG
#        # target_button = 1,2;        # PCL Program sets this.
#    } E_Comp_Question;
#} T_Comp_Question;


# Definition of the Trial to send a code indicating that a Timeout has occurred.
/* When a button-pressed response occurs, 1 or 2 is sent to EEG depending upon the button pressed.
   When a timeout occurs, nothing gets sent.  Just in order to have equal number of lines in the 
   trigger files later, it's better to send a code indicating timeout. 
   (Yes, IN ADDITION to the usual code 199 that will follow.)
*/
#trial
#{
#    all_responses = false;
    
#    stimulus_event
#    {
#        nothing {};                  # This is the default picture, which is nothing!
#		  time = 15;
#		  duration = 100;
#    } E_Report_Timeout_to_EEG_99;    

#    stimulus_event
#    {
#        nothing {};                  # This is the default picture, which is nothing!
#		  deltat = 100;
#		  duration = 20;
#    } E_Report_Timeout_to_EEG_199;    

#} T_Report_Timeout_to_EEG;


# Definition of the Trial to send the response code to EEG
#trial
#{
#    trial_duration = 100;
#    all_responses = false;
    
#    stimulus_event
#    {
#        nothing {};                 # Basically, nothing is done!
#        time = 90;     
        /* Start this event 15 milliseconds after the trial starts.  This is to provide a delay in
         sending the code to EEG to avoid trying to reach the port when it is still busy
           having just received the response 1 or 2 from the button box, or the timeout code.
        */
#        # code = "Send Code";      #  PCL Program fills this!!!
#        # port_code = 196, 197 or 199, PCL Program fills this!!!
#    } E_Response_Code_to_EEG;    
#} T_Response_Code_to_EEG;


# Definition of the Trial to show a Pause
# subject Controlled
#trial
#{
#    trial_duration = forever;
#    trial_type = specific_response;
#    terminator_button = 3;

#    stimulus_event
#    {
#        picture P_Pause; 
#        code = "Pause";                   
#    };   
#} T_Pause;


# Definition of the Trial to Interrupt in the middle of a session
# Operator Controlled
trial
{
    trial_duration = forever;
    trial_type = specific_response;
    terminator_button = 2;

    stimulus_event
    {
        picture P_Interruption; 
        code = "Kleine Unterbrechung!";                   
    };   
} T_Interruption;

# Definition of the Trial to end the session
# Operator Controlled
trial
{
    trial_duration = forever;
    all_responses = false;
    trial_type = first_response;

    stimulus_event
    {
        picture P_End_Thanks;
        code = "Ende der Session!";
    };
    picture P_End_Thanks;
} T_End_Thanks;

##############################################################################
# Presentation Control Language (PCL) Program Part
##############################################################################

begin_pcl;

int N_of_Trials = 1;                                 # Number of Experimental Trials per Session.  
int N_of_Blocks = 1;                                 # Number of Blocks per Session.                       
int N_of_Trials_per_Block = N_of_Trials/N_of_Blocks; # Number of Experimental Trials per Block.

#preset string V_Version; # Prompt the version at the beginning!!!


#input_file F_Session_List = new input_file;
#F_Session_List.open("Session_List_" + V_Version + ".txt");  
# This is the file containing the list of stimuli for one session.
# Each line in this file must correspond to one experimental trial.

####  Specific for presenting Visual Stimuli   --------------------------------

#input_file F_Visual_Stimulus = new input_file;
#F_Visual_Stimulus.open("Visual_Stimulus_" + V_Version + ".txt");
#string V_Visual_Stimulus; 
#array<string> A_Stimulus_Words[0];

####  Specific for presenting Visual Stimuli + Text Comprehension Question  ---

#int V_Current_Word;
#int V_Underscore_Index;
#int V_Star_Index;

####  Specific for presenting Comprehension Questions from Textfile   ---------

#input_file F_Comp_Question = new input_file;
#F_Comp_Question.open("Comp_Question_List_" + V_Version + ".txt");
#array<string> A_Comp_Question_Words[0];

####

# Columns of strings in the order found in the input Session List file.
#string V_Stimulus_Sentence;
#string V_Context_Question;   # If any, otherwise dummy for visual experiments!!!
#string V_Item;
#string V_Condition;
#string V_Condition_Code;
#string V_Comp_Question;
#string V_Expected_Response;

# output_port O_Port_to_EEG = output_port_manager.get_port(1);    
#stimulus_data D_Stimulus_Data = stimulus_manager.last_stimulus_data();

#========================
# Main Experiment Loop: -
#========================
loop     /*** Begin Main Loop 1 for Blocks ***/
    int V_Current_Block = 1

until  
    V_Current_Block > N_of_Blocks

begin
    if V_Current_Block == 1 then
	    T_Start_Exp.present();
	    T_Blank_Screen.present();
	    T_Launch_New_Trial.present(); 
	    T_Blank_Screen.present();
	    T_Text_Display.present();
    else
       T_Continue.present();
       T_Blank_Screen.present();
    end;
    T_Blank_Screen.present();
    T_End_Thanks.present();
    T_Blank_Screen.present();
    

    #==========
    # Loop 2: -
    #==========
#    loop    /*** Begin Loop 2 for Trials ***/
#        int V_Current_Trial = 1

#    until
#        V_Current_Trial > N_of_Trials_per_Block

#    begin
#        V_Stimulus_Sentence = F_Session_List.get_string(); # Eg: 01SAI
#        V_Context_Question = F_Session_List.get_string();  # Eg: CQ01SAI # If any!!! Or use Dummy!!!
#        V_Item = F_Session_List.get_string();              # Eg: 01
        #V_Condition = F_Session_List.get_string();         # Eg: SAI
#        V_Condition_Code = F_Session_List.get_string();    # Eg: 230
        #V_Comp_Question = F_Session_List.get_string();     # Eg: Q01SAIC
        #V_Expected_Response = F_Session_List.get_string(); # Eg: 2 (Left button => Correct)

        # Do filename and event-code assignments valid during this exp-trial;
        # Load wavefile and bitmap stimuli that are needed for this trial;
        /* We do this before launching the experimental trial so as to keep
           as minimal intereference as possible in trial timings.  You see,
           these things consume processor time, however small they are!
        */

#        E_Launch_New_Trial.set_event_code("B" + string(V_Current_Block) + " T" + string(V_Current_Trial));
#        E_Comp_Question.set_target_button(int(V_Expected_Response));
       
        # O_Port_to_EEG.send_code(int(V_Item));               # Send Item number to EEG
        E_Launch_New_Trial.set_event_code(string (255));
        #E_Launch_New_Trial.set_port_code(255);
        T_Launch_New_Trial.present();                         # Show Star for 400 ms
     
        /*****************  Visual Stimulus Presentation Begins  ***********************/
       
 #       T_Blank_Screen.set_duration(200);
        
#        A_Stimulus_Words.resize(0);                       # Empty the array of words.
#        V_Visual_Stimulus = F_Visual_Stimulus.get_line(); # Read one line at a time from the file.

        # Split it word by word.  If two words need to be together, there must be an _ in the input!
#        V_Visual_Stimulus.split(" ",A_Stimulus_Words);
    
        # Present the words (or word pairs or word triplets) one by one.
        #============
        # Loop 2.1: -
        #============        
 #       loop    /*** Begin Loop 2.1 for Stimulus Words ***/
 #           V_Current_Word = 2 # Leave the first one out, because that's the Stimulus Number or Index or so.
 #       until
 #           V_Current_Word > A_Stimulus_Words.count()
 #       begin
    

 #           if V_Current_Word == 2 then
 #               #E_Visual_Stimulus.set_port_code(int(V_Item));              # set an EEG-Trigger when the first word is presented
 #               E_Text_Display.set_event_code(V_Stimulus_Sentence);     # set an entry to log-file when the first word is presented
#				else
	#				E_Visual_Stimulus.set_event_code("");
#					#E_Visual_Stimulus.set_port_code(0);
#				end;


            /* Find if there's an underscore in the current array element somewhere.
               If there's one, replace it with a space (ASCII Char 32) before presenting the words.
               An underscore means that the words need to be presented together, as in 'Of course'!
            */
 #           V_Underscore_Index = A_Stimulus_Words[V_Current_Word].find("_");
 #           if V_Underscore_Index != 0 then
 #               A_Stimulus_Words[V_Current_Word].set_char(V_Underscore_Index, 32); # 32 -> Space
 #               T_Visual_Stimulus.set_duration(400);              # Show double words for 400 ms
 #           end;

            /* Do the same thing again, if you thing you'll have to present 3 words at a time.
               But comment this out if you don't have triplets.  Saves time!!!
            */
 #           V_Underscore_Index = A_Stimulus_Words[V_Current_Word].find("_");
 #           if V_Underscore_Index != 0 then
 #               A_Stimulus_Words[V_Current_Word].set_char(V_Underscore_Index, 32);
 #               T_Visual_Stimulus.set_duration(450);  # Show triplets for 450 ms
 #           end;
            
            /* If there are critical words, at the beginning of which a trigger needs to be sent,
                these will be indicated with a star in the Visual-Stimulus text file.  However,
                when actually showing the word, this must be replaced by a space.  We do this,
                and send an event code to the logfile as well as a port code to EEG.
            */
            #V_Star_Index = A_Stimulus_Words[V_Current_Word].find("*");
            #if V_Star_Index != 0 then
            #    A_Stimulus_Words[V_Current_Word].set_char(V_Star_Index, 32);
            #    E_Visual_Stimulus.set_event_code(V_Condition_Code);       
#                E_Visual_Stimulus.set_event_code("Critical Word");
#              # E_Visual_Stimulus.set_port_code(int(V_Item) + 100);
               # E_Visual_Stimulus.set_port_code(int(V_Condition_Code));

            #end;
            
            # Present Blank Screen between words
 #           T_Blank_Screen.present();
            
            # Put the current word on the picture and redraw the text part in it.
  #          Txt_Visual_Stimulus.set_caption(A_Stimulus_Words[V_Current_Word]);
  #          Txt_Visual_Stimulus.redraw();

            # Present the trial that will show the changed text on screen for x ms.
 #           T_Text_Display.present();
            
            # Reset things before going to the next word!
#            E_Visual_Stimulus.set_event_code("");
#            E_Visual_Stimulus.set_port_code(port_code_none);
 #           T_Visual_Stimulus.set_duration(300);
            
            # Go to the next word.
 #           V_Current_Word = V_Current_Word + 1;
        
        #=============
        # End Loop 2.1
        #=============
#        end; /*** End Loop 2.1 for Stimulus Words ***/
        /*****************  Visual Stimulus Presentation Ends  *************************/

        # Duration for the Blank Screen between last stimulus word and ???
#        T_Blank_Screen.set_duration(500);
#        T_Blank_Screen.present();
        # Present the trial that will show the recomposed Comprehension Question on screen for x ms.
 #       T_Show_Question_Mark.present();
        
        /*****************  Comprehension Question Set-up Begins  **********************/
        
 #       A_Comp_Question_Words.resize(0);              # Empty the array of words.
 #       V_Comp_Question = F_Comp_Question.get_line(); # Read one line at a time from the file.

        # Split it word by word.  If two words need to be together, there must be an '_' in the input!
 #       V_Comp_Question.split(" ",A_Comp_Question_Words);
 #       V_Comp_Question = "";                         # Empty it to recompose it later. 
       
        # Set up or Compose the Comprehension Question.
        #============
        # Loop 2.2: -
        #============        
#        loop    /*** Begin Loop 2.2 for Comprehension Question Words ***/
#            V_Current_Word = 2 # Leave the first one out, because that's the Question Number or Index or so.
#        until
#            V_Current_Word > A_Comp_Question_Words.count()
#        begin
        
            # Find if there's an underscore in the current array element somewhere.
            # If there's one, replace it with a space (ASCII Char 32) before presenting the words.

#            V_Underscore_Index = A_Comp_Question_Words[V_Current_Word].find("_");
#            if V_Underscore_Index != 0 then
#                A_Comp_Question_Words[V_Current_Word].set_char(V_Underscore_Index, 32);
#            end;
            
            # Recompose the Comprehension Question with an newline character every 4 words!
            # Remember, we left the first word out, because it is the index.  So 5, 9, 13 etc.
            # Also, we don't want to introduce a 'return' character, if it is the last word of the array.

#            if ((V_Current_Word == 5) || (V_Current_Word == 9) || (V_Current_Word == 13)) && (V_Current_Word != A_Comp_Question_Words.count()) then
#                V_Comp_Question = V_Comp_Question + A_Comp_Question_Words[V_Current_Word] + "\n";
#            else
#                V_Comp_Question = V_Comp_Question + A_Comp_Question_Words[V_Current_Word] + " ";
#            end;

            # Go to the next word.
#            V_Current_Word = V_Current_Word + 1;

        #=============
        # End Loop 2.2
        #=============
 #       end; /*** End Loop 2.2 for Comprehension Question Words ***/    

        /*****************  Comprehension Question Set-up Ends  ************************/

        # Put the recomposed Comprehension Question on the picture and redraw the text part in it.
 #       Txt_Comp_Question.set_caption(V_Comp_Question);
 #       Txt_Comp_Question.redraw();
        
 #       T_Blank_Screen.present();
        # Present the trial that will show the recomposed Comprehension Question on screen for x ms.
 #       T_Comp_Question.present();

        # Monitor the Response and send appropriate port codes to EEG!!!
       # D_Stimulus_Data = stimulus_manager.last_stimulus_data();
        
        # To delay sending the following codes to the Output Port!!!
        # Because these are almost at the same time as the Response there, so the port
        # cannot handle it unless there's a short delay.
       # if (D_Stimulus_Data.type() == stimulus_miss) then
			#	E_Report_Timeout_to_EEG_99.set_port_code(99);
			#	E_Report_Timeout_to_EEG_99.set_event_code(string (99));
			#	E_Report_Timeout_to_EEG_199.set_port_code(199);
			#	E_Report_Timeout_to_EEG_199.set_event_code(string (199));
         #  T_Report_Timeout_to_EEG.present();
            
        #elseif (D_Stimulus_Data.type() == stimulus_incorrect) then
         #   E_Response_Code_to_EEG.set_port_code(197);
			#	E_Response_Code_to_EEG.set_event_code(string (197));
          #  T_Response_Code_to_EEG.present();
            
        #elseif (D_Stimulus_Data.type() == stimulus_hit) then
         #   E_Response_Code_to_EEG.set_port_code(196);
			#	E_Response_Code_to_EEG.set_event_code(string (196));
         #   T_Response_Code_to_EEG.present();        
        
        #end;
        
		  # Reset Duration for the Blank Screen
 #       T_Blank_Screen.set_duration(1500);
 #       T_Blank_Screen.present();
        
        # Go to the Next Trial
#        V_Current_Trial = V_Current_Trial + 1;
    
    #===========
    # End Loop 2
    #===========
 #   end; /*** End Loop 2 for Trials ***/

# Go to the Next Block
#V_Current_Block = V_Current_Block + 1;

#================
# End Main Loop 1 
#================
end;    /*** End Main Loop 1 for Blocks ***/

# Close all the open files
#F_Session_List.close(); 
#F_Visual_Stimulus.close();
#F_Comp_Question.close();

# Finish off the session!!!
T_End_Thanks.present();