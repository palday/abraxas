232VIPIPS Durch_das_Holzgestell *kracht *ein_Hagelsturm, bevor es völlig in sich zusammenbricht.
226VAPIPS Aus_der_Badewanne *krabbelt *eine_Schaumkrone, bevor das_Badewasser abgelassen_wird.
220VAPAPS Durch_die_Menschenmenge *drängt *ein_Bäcker, bevor der_Versammlungsplatz geräumt_wird.
230VIPAES Es *schlingert *ein_Briefträger um_die_Ecke, bevor er auf_dem_Gehweg stehen_bleibt.
214VAPAPS Über_die_Tribüne *flitzt *ein_Platzwart, bevor das_Sportereignis eröffnet_wird.
221VAPAES Es *schnellt *ein_Techniker in_die_Bühnenmitte, obwohl die_Vorstellung schon begonnen_hat.
211VAPIPS Über_die_Rampe *balanciert *ein_Schaufelbagger, obwohl es in_der_Halle stockdunkel ist.
229VAPIES Es *huscht *ein_Schlitten über_den_Schneehügel, bevor er auf_vereistem_Boden ins_Rutschen kommt.
234VIPAES Es *eiert *eine_Konditorin durch_den_Supermarkt, solange keine_Hindernisse im_Weg stehen.
220VIPIPS Durch_die_Menschenmenge *schwingt *eine_Unruhe, bevor der_Versammlungsplatz geräumt_wird.
225VAPAES Es *zuckelt *ein_Braunbär in_die_Grube, obwohl sie Tag und Nacht bewacht_wird.
223VAPAPS Aus_dem_Loch *schlüpft *eine_Maus, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
210VAPIES Es *eilt *eine_Pressemeldung durch_die_Redaktion, solange die_Nachricht frisch ist.
232VAPAES Es *tobt *ein_Schimpanse durch_das_Holzgestell, bevor es völlig in sich zusammenbricht.
216VIPAPS Über_den_Rasen *kugelt *ein_Stürmer, nachdem das_Gras frisch gestutzt_worden_war.
207VAPIPS Über_den_Wald *fliegt *eine_Wolke, solange der_Wind aus_westlicher_Richtung weiter anhält.
217VAPAPS In_den_Teich *rast *ein_Gärtner, obwohl ein_stabiles_Absperrband rundherum führt.
205VAPIES Es *steigt *ein_Strauch über_den_Zaun, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
219VAPAES Es *paddelt *eine_Wildente durch_die_Stromschnelle, solange genug Wasser hindurchfließt.
202VIPAPS Über_den_See *driftet *ein_Tourist, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
229VAPIPS Über_den_Schneehügel *huscht *ein_Schlitten, bevor er auf_vereistem_Boden ins_Rutschen kommt.
211VAPIES Es *balanciert *ein_Schaufelbagger über_die_Rampe, obwohl es in_der_Halle stockdunkel ist.
235VAPIES Es *schwirrt *ein_Laserlicht durch_die_Nacht, während der_Mond am_Himmel leuchtet.
228VAPIPS Auf_die_Verkehrsinsel *hechtet *ein_Kleinwagen, während ein_Passant am_Straßenrand steht und gafft.
210VIPAES Es *zirkuliert *ein_Auszubildender durch_die_Redaktion, solange die_Nachricht frisch ist.
227VIPIES Es *klatscht *ein_Frühstücksei auf_den_Teller, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
224VIPIES Es *kreiselt *ein_Snowboard über_den_Gletscher, nachdem sich der_Morgennebel verzogen_hat.
233VIPIPS Durch_die_Dachluke *bricht *ein_Ast, nachdem das_Dach neu gedeckt_worden_war.
209VAPIPS Aus_dem_Bett *hopst *ein_Kissen, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
203VAPAES Es *taucht *ein_Pinguin unter_die_Wasseroberfläche, bevor ein_Hund hinterherspringt.
216VAPIES Es *tänzelt *ein_Fußball über_den_Rasen, nachdem das_Gras frisch gestutzt_worden_war.
224VIPAES Es *kreiselt *ein_Murmeltier über_den_Gletscher, nachdem sich der_Morgennebel verzogen_hat.
229VIPAPS Über_den_Schneehügel *strauchelt *ein_Leopard, bevor er auf_vereistem_Boden ins_Rutschen kommt.
213VAPAPS Durch_die_Windböe *jagt *eine_Rauchschwalbe, während sich alle_Leute ins_Warme flüchten.
234VAPIPS Durch_den_Supermarkt *kurvt *eine_Blechdose, solange keine_Hindernisse im_Weg stehen.
233VAPAES Es *klettert *ein_Clown durch_die_Dachluke, nachdem das_Dach neu gedeckt_worden_war.
225VIPAPS In_die_Grube *sackt *ein_Braunbär, obwohl sie Tag und Nacht bewacht_wird.
226VIPAES Es *glitscht *ein_Hotelgast aus_der_Badewanne, bevor das_Badewasser abgelassen_wird.
236VIPIES Es *kullert *ein_Stein über_die_Wiese, während die_Vögel in_den_Bäumen zwitschern.
212VIPAES Es *kursiert *ein_Reisender durch_die_Wohnanlage, solange das von_niemandem beachtet_wird.
231VAPIPS Über_die_Sandbahn *rennt *eine_Münze, während nebenan einige_Schüler Basketball spielen.
232VIPAES Es *kracht *ein_Schimpanse durch_das_Holzgestell, bevor es völlig in sich zusammenbricht.
229VIPIES Es *strauchelt *ein_Schlitten über_den_Schneehügel, bevor er auf_vereistem_Boden ins_Rutschen kommt.
213VAPAES Es *jagt *eine_Rauchschwalbe durch_die_Windböe, während sich alle_Leute ins_Warme flüchten.
203VIPIPS Unter_die_Wasseroberfläche *sinkt *ein_Betonklotz, bevor ein_Hund hinterherspringt.
226VAPAES Es *krabbelt *ein_Hotelgast aus_der_Badewanne, bevor das_Badewasser abgelassen_wird.
214VAPAES Es *flitzt *ein_Platzwart über_die_Tribüne, bevor das_Sportereignis eröffnet_wird.
212VAPAPS Durch_die_Wohnanlage *vagabundiert *ein_Reisender, solange das von_niemandem beachtet_wird.
215VAPAES Es *pflügt *ein_Buckelwal durch_die_Welle, solange sie vom_Sturm über_das_Meer getrieben_wird.
233VIPIES Es *bricht *ein_Ast durch_die_Dachluke, nachdem das_Dach neu gedeckt_worden_war.
223VIPIPS Aus_dem_Loch *quillt *eine_Perle, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
226VIPAPS Aus_der_Badewanne *glitscht *ein_Hotelgast, bevor das_Badewasser abgelassen_wird.
217VAPIES Es *rast *ein_Fass in_den_Teich, obwohl ein_stabiles_Absperrband rundherum führt.
218VIPIES Es *platscht *ein_Ölteppich in_die_Kanalisation, während der_Kanaldeckel ausgetauscht_wird.
228VIPAPS Auf_die_Verkehrsinsel *prallt *ein_Fußgänger, während ein_Passant am_Straßenrand steht und gafft.
235VIPIES Es *zuckt *ein_Laserlicht durch_die_Nacht, während der_Mond am_Himmel leuchtet.
232VAPIPS Durch_das_Holzgestell *tobt *ein_Hagelsturm, bevor es völlig in sich zusammenbricht.
207VAPIES Es *fliegt *eine_Wolke über_den_Wald, solange der_Wind aus_westlicher_Richtung weiter anhält.
225VAPIPS In_die_Grube *zuckelt *ein_Bauwagen, obwohl sie Tag und Nacht bewacht_wird.
215VIPAPS Durch_die_Welle *wogt *ein_Buckelwal, solange sie vom_Sturm über_das_Meer getrieben_wird.
204VIPAPS Durch_die_Manege *pendelt *ein_Turner, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
231VAPAES Es *rennt *ein_Igel über_die_Sandbahn, während nebenan einige_Schüler Basketball spielen.
223VAPIES Es *schlüpft *eine_Perle aus_dem_Loch, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
215VAPAPS Durch_die_Welle *pflügt *ein_Buckelwal, solange sie vom_Sturm über_das_Meer getrieben_wird.
207VAPAES Es *fliegt *ein_Falke über_den_Wald, solange der_Wind aus_westlicher_Richtung weiter anhält.
209VIPIES Es *plumpst *ein_Kissen aus_dem_Bett, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
201VAPAES Es *tanzt *eine_Ballerina über_das_Parkett, während die_Empore des_Saals geputzt_wird.
222VIPIPS Über_das_Schiff *schleift *ein_Eimer, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
208VAPIPS Über_die_Gleise *stampft *eine_Lokomotive, nachdem die_Schranke geschlossen_wurde.
221VIPIES Es *knallt *eine_Kulisse in_die_Bühnenmitte, obwohl die_Vorstellung schon begonnen_hat.
211VIPIES Es *wankt *ein_Schaufelbagger über_die_Rampe, obwohl es in_der_Halle stockdunkel ist.
228VIPAES Es *prallt *ein_Fußgänger auf_die_Verkehrsinsel, während ein_Passant am_Straßenrand steht und gafft.
218VIPAPS In_die_Kanalisation *platscht *eine_Schnecke, während der_Kanaldeckel ausgetauscht_wird.
216VAPAPS Über_den_Rasen *tänzelt *ein_Stürmer, nachdem das_Gras frisch gestutzt_worden_war.
204VIPIPS Durch_die_Manege *pendelt *ein_Seil, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
208VIPAES Es *holpert *ein_Mechaniker über_die_Gleise, nachdem die_Schranke geschlossen_wurde.
205VIPIES Es *knickt *ein_Strauch über_den_Zaun, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
212VIPIES Es *kursiert *eine_Postkarte durch_die_Wohnanlage, solange das von_niemandem beachtet_wird.
221VAPAPS In_die_Bühnenmitte *schnellt *ein_Techniker, obwohl die_Vorstellung schon begonnen_hat.
210VAPIPS Durch_die_Redaktion *eilt *eine_Pressemeldung, solange die_Nachricht frisch ist.
206VIPAPS Durch_die_Wüste *treibt *ein_Hirte, nachdem alle_Pflanzen schon lange ausgestorben_waren.
201VAPIES Es *tanzt *ein_Sonnenstrahl über_das_Parkett, während die_Empore des_Saals geputzt_wird.
202VAPAES Es *schwimmt *ein_Tourist über_den_See, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
206VIPIES Es *treibt *eine_Düne durch_die_Wüste, nachdem alle_Pflanzen schon lange ausgestorben_waren.
230VIPAPS Um_die_Ecke *schlingert *ein_Briefträger, bevor er auf_dem_Gehweg stehen_bleibt.
220VAPAES Es *drängt *ein_Bäcker durch_die_Menschenmenge, bevor der_Versammlungsplatz geräumt_wird.
224VAPAPS Über_den_Gletscher *wuselt *ein_Murmeltier, nachdem sich der_Morgennebel verzogen_hat.
213VAPIES Es *jagt *eine_Schneeflocke durch_die_Windböe, während sich alle_Leute ins_Warme flüchten.
204VIPAES Es *pendelt *ein_Turner durch_die_Manege, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
217VIPIES Es *kippt *ein_Fass in_den_Teich, obwohl ein_stabiles_Absperrband rundherum führt.
205VIPAPS Über_den_Zaun *knickt *ein_Passant, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
231VIPIPS Über_die_Sandbahn *rollt *eine_Münze, während nebenan einige_Schüler Basketball spielen.
235VAPIPS Durch_die_Nacht *schwirrt *ein_Laserlicht, während der_Mond am_Himmel leuchtet.
215VIPIES Es *wogt *ein_Surfbrett durch_die_Welle, solange sie vom_Sturm über_das_Meer getrieben_wird.
205VIPIPS Über_den_Zaun *knickt *ein_Strauch, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
213VIPAPS Durch_die_Windböe *wirbelt *eine_Rauchschwalbe, während sich alle_Leute ins_Warme flüchten.
233VAPIPS Durch_die_Dachluke *klettert *ein_Ast, nachdem das_Dach neu gedeckt_worden_war.
223VIPIES Es *quillt *eine_Perle aus_dem_Loch, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
230VAPIPS Um_die_Ecke *biegt *eine_Schubkarre, bevor sie auf_dem_Gehweg stehen_bleibt.
211VAPAPS Über_die_Rampe *balanciert *ein_Lagerarbeiter, obwohl es in_der_Halle stockdunkel ist.
219VAPAPS Durch_die_Stromschnelle *paddelt *eine_Wildente, solange genug Wasser hindurchfließt.
234VAPIES Es *kurvt *eine_Blechdose durch_den_Supermarkt, solange keine_Hindernisse im_Weg stehen.
206VIPAES Es *treibt *ein_Hirte durch_die_Wüste, nachdem alle_Pflanzen schon lange ausgestorben_waren.
236VAPIPS Über_die_Wiese *hüpft *ein_Stein, während die_Vögel in_den_Bäumen zwitschern.
212VAPIPS Durch_die_Wohnanlage *vagabundiert *eine_Postkarte, solange das von_niemandem beachtet_wird.
202VIPAES Es *driftet *ein_Tourist über_den_See, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
218VIPAES Es *platscht *eine_Schnecke in_die_Kanalisation, während der_Kanaldeckel ausgetauscht_wird.
219VAPIES Es *paddelt *ein_Gummiboot durch_die_Stromschnelle, solange genug Wasser hindurchfließt.
224VIPIPS Über_den_Gletscher *kreiselt *ein_Snowboard, nachdem sich der_Morgennebel verzogen_hat.
206VIPIPS Durch_die_Wüste *treibt *eine_Düne, nachdem alle_Pflanzen schon lange ausgestorben_waren.
230VIPIES Es *schlingert *eine_Schubkarre um_die_Ecke, bevor sie auf_dem_Gehweg stehen_bleibt.
214VIPAPS Über_die_Tribüne *schwankt *ein_Platzwart, bevor das_Sportereignis eröffnet_wird.
210VAPAPS Durch_die_Redaktion *eilt *ein_Auszubildender, solange die_Nachricht frisch ist.
219VIPIPS Durch_die_Stromschnelle *rotiert *ein_Gummiboot, solange genug Wasser hindurchfließt.
225VAPIES Es *zuckelt *ein_Bauwagen in_die_Grube, obwohl sie Tag und Nacht bewacht_wird.
220VIPAES Es *schwingt *ein_Bäcker durch_die_Menschenmenge, bevor der_Versammlungsplatz geräumt_wird.
236VIPIPS Über_die_Wiese *kullert *ein_Stein, während die_Vögel in_den_Bäumen zwitschern.
222VAPIES Es *poltert *ein_Eimer über_das_Schiff, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
217VIPAPS In_den_Teich *kippt *ein_Gärtner, obwohl ein_stabiles_Absperrband rundherum führt.
209VAPAES Es *hopst *ein_Kleinkind aus_dem_Bett, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
222VAPAPS Über_das_Schiff *poltert *ein_Pirat, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
228VAPIES Es *hechtet *ein_Kleinwagen auf_die_Verkehrsinsel, während ein_Passant am_Straßenrand steht und gafft.
207VAPAPS Über_den_Wald *fliegt *ein_Falke, solange der_Wind aus_westlicher_Richtung weiter anhält.
203VIPAPS Unter_die_Wasseroberfläche *sinkt *ein_Pinguin, bevor ein_Hund hinterherspringt.
227VIPAPS Auf_den_Teller *klatscht *ein_Marienkäfer, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
222VIPAES Es *schleift *ein_Pirat über_das_Schiff, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
201VIPIPS Über_das_Parkett *gleitet *ein_Sonnenstrahl, während die_Empore des_Saals geputzt_wird.
218VAPAPS In_die_Kanalisation *kriecht *eine_Schnecke, während der_Kanaldeckel ausgetauscht_wird.
234VIPIPS Durch_den_Supermarkt *eiert *eine_Blechdose, solange keine_Hindernisse im_Weg stehen.
203VIPIES Es *sinkt *ein_Betonklotz unter_die_Wasseroberfläche, bevor ein_Hund hinterherspringt.
235VIPIPS Durch_die_Nacht *zuckt *ein_Laserlicht, während der_Mond am_Himmel leuchtet.
208VAPAPS Über_die_Gleise *stampft *ein_Mechaniker, nachdem die_Schranke geschlossen_wurde.
204VAPIES Es *schaukelt *ein_Seil durch_die_Manege, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
201VIPAPS Über_das_Parkett *gleitet *eine_Ballerina, während die_Empore des_Saals geputzt_wird.
209VAPAPS Aus_dem_Bett *hopst *ein_Kleinkind, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
227VAPAES Es *läuft *ein_Marienkäfer auf_den_Teller, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
214VIPAES Es *schwankt *ein_Platzwart über_die_Tribüne, bevor das_Sportereignis eröffnet_wird.
231VAPIES Es *rennt *eine_Münze über_die_Sandbahn, während nebenan einige_Schüler Basketball spielen.
221VIPIPS In_die_Bühnenmitte *knallt *eine_Kulisse, obwohl die_Vorstellung schon begonnen_hat.
236VIPAES Es *kullert *ein_Junge über_die_Wiese, während die_Vögel in_den_Bäumen zwitschern.
202VIPIPS Über_den_See *driftet *ein_Frachter, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
208VAPAES Es *stampft *ein_Mechaniker über_die_Gleise, nachdem die_Schranke geschlossen_wurde.
216VIPAES Es *kugelt *ein_Stürmer über_den_Rasen, nachdem das_Gras frisch gestutzt_worden_war.
227VAPIPS Auf_den_Teller *läuft *ein_Frühstücksei, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
