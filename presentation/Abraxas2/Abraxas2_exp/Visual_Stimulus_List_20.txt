212VIPIPS Durch_die_Wohnanlage *kursiert *eine_Postkarte, solange das von_niemandem beachtet_wird.
209VIPIPS Aus_dem_Bett *plumpst *ein_Kissen, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
230VIPIES Es *schlingert *eine_Schubkarre um_die_Ecke, bevor sie auf_dem_Gehweg stehen_bleibt.
215VIPAES Es *wogt *ein_Buckelwal durch_die_Welle, solange sie vom_Sturm über_das_Meer getrieben_wird.
222VAPAES Es *poltert *ein_Pirat über_das_Schiff, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
203VIPAPS Unter_die_Wasseroberfläche *sinkt *ein_Pinguin, bevor ein_Hund hinterherspringt.
225VIPAES Es *sackt *ein_Braunbär in_die_Grube, obwohl sie Tag und Nacht bewacht_wird.
201VIPIPS Über_das_Parkett *gleitet *ein_Sonnenstrahl, während die_Empore des_Saals geputzt_wird.
212VIPIES Es *kursiert *eine_Postkarte durch_die_Wohnanlage, solange das von_niemandem beachtet_wird.
203VAPAES Es *taucht *ein_Pinguin unter_die_Wasseroberfläche, bevor ein_Hund hinterherspringt.
214VIPIPS Über_die_Tribüne *schwankt *ein_Lichtkegel, bevor das_Sportereignis eröffnet_wird.
219VIPAPS Durch_die_Stromschnelle *rotiert *eine_Wildente, solange genug Wasser hindurchfließt.
228VAPAES Es *hechtet *ein_Fußgänger auf_die_Verkehrsinsel, während ein_Passant am_Straßenrand steht und gafft.
205VAPIES Es *steigt *ein_Strauch über_den_Zaun, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
236VIPIPS Über_die_Wiese *kullert *ein_Stein, während die_Vögel in_den_Bäumen zwitschern.
211VAPIES Es *balanciert *ein_Schaufelbagger über_die_Rampe, obwohl es in_der_Halle stockdunkel ist.
206VAPIES Es *wandert *eine_Düne durch_die_Wüste, nachdem alle_Pflanzen schon lange ausgestorben_waren.
223VAPAPS Aus_dem_Loch *schlüpft *eine_Maus, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
235VIPAES Es *zuckt *eine_Libelle durch_die_Nacht, während der_Mond am_Himmel leuchtet.
214VAPAES Es *flitzt *ein_Platzwart über_die_Tribüne, bevor das_Sportereignis eröffnet_wird.
210VAPAES Es *eilt *ein_Auszubildender durch_die_Redaktion, solange die_Nachricht frisch ist.
234VIPIPS Durch_den_Supermarkt *eiert *eine_Blechdose, solange keine_Hindernisse im_Weg stehen.
226VIPAPS Aus_der_Badewanne *glitscht *ein_Hotelgast, bevor das_Badewasser abgelassen_wird.
231VIPAES Es *rollt *ein_Igel über_die_Sandbahn, während nebenan einige_Schüler Basketball spielen.
225VAPAPS In_die_Grube *zuckelt *ein_Braunbär, obwohl sie Tag und Nacht bewacht_wird.
216VIPIES Es *kugelt *ein_Fußball über_den_Rasen, nachdem das_Gras frisch gestutzt_worden_war.
202VAPAES Es *schwimmt *ein_Tourist über_den_See, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
228VIPAPS Auf_die_Verkehrsinsel *prallt *ein_Fußgänger, während ein_Passant am_Straßenrand steht und gafft.
212VAPIES Es *vagabundiert *eine_Postkarte durch_die_Wohnanlage, solange das von_niemandem beachtet_wird.
216VIPAPS Über_den_Rasen *kugelt *ein_Stürmer, nachdem das_Gras frisch gestutzt_worden_war.
221VAPIPS In_die_Bühnenmitte *schnellt *eine_Kulisse, obwohl die_Vorstellung schon begonnen_hat.
201VIPAES Es *gleitet *eine_Ballerina über_das_Parkett, während die_Empore des_Saals geputzt_wird.
236VAPIES Es *hüpft *ein_Stein über_die_Wiese, während die_Vögel in_den_Bäumen zwitschern.
213VIPAES Es *wirbelt *eine_Rauchschwalbe durch_die_Windböe, während sich alle_Leute ins_Warme flüchten.
235VAPAPS Durch_die_Nacht *schwirrt *eine_Libelle, während der_Mond am_Himmel leuchtet.
218VIPAPS In_die_Kanalisation *platscht *eine_Schnecke, während der_Kanaldeckel ausgetauscht_wird.
234VIPIES Es *eiert *eine_Blechdose durch_den_Supermarkt, solange keine_Hindernisse im_Weg stehen.
228VAPAPS Auf_die_Verkehrsinsel *hechtet *ein_Fußgänger, während ein_Passant am_Straßenrand steht und gafft.
209VAPIPS Aus_dem_Bett *hopst *ein_Kissen, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
201VIPIES Es *gleitet *ein_Sonnenstrahl über_das_Parkett, während die_Empore des_Saals geputzt_wird.
223VIPAES Es *quillt *eine_Maus aus_dem_Loch, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
221VAPAPS In_die_Bühnenmitte *schnellt *ein_Techniker, obwohl die_Vorstellung schon begonnen_hat.
203VAPIPS Unter_die_Wasseroberfläche *taucht *ein_Betonklotz, bevor ein_Hund hinterherspringt.
235VIPIPS Durch_die_Nacht *zuckt *ein_Laserlicht, während der_Mond am_Himmel leuchtet.
204VIPIES Es *pendelt *ein_Seil durch_die_Manege, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
226VAPAPS Aus_der_Badewanne *krabbelt *ein_Hotelgast, bevor das_Badewasser abgelassen_wird.
220VAPAES Es *drängt *ein_Bäcker durch_die_Menschenmenge, bevor der_Versammlungsplatz geräumt_wird.
207VAPIPS Über_den_Wald *fliegt *eine_Wolke, solange der_Wind aus_westlicher_Richtung weiter anhält.
229VAPAPS Über_den_Schneehügel *huscht *ein_Leopard, bevor er auf_vereistem_Boden ins_Rutschen kommt.
225VIPIES Es *sackt *ein_Bauwagen in_die_Grube, obwohl sie Tag und Nacht bewacht_wird.
228VIPIES Es *prallt *ein_Kleinwagen auf_die_Verkehrsinsel, während ein_Passant am_Straßenrand steht und gafft.
209VAPAES Es *hopst *ein_Kleinkind aus_dem_Bett, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
224VAPAPS Über_den_Gletscher *wuselt *ein_Murmeltier, nachdem sich der_Morgennebel verzogen_hat.
231VIPIES Es *rollt *eine_Münze über_die_Sandbahn, während nebenan einige_Schüler Basketball spielen.
205VIPAES Es *knickt *ein_Passant über_den_Zaun, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
214VIPAPS Über_die_Tribüne *schwankt *ein_Platzwart, bevor das_Sportereignis eröffnet_wird.
209VIPAES Es *plumpst *ein_Kleinkind aus_dem_Bett, obwohl das_Bett von_einem niedrigen_Gitter umrahmt_wird.
208VAPIPS Über_die_Gleise *stampft *eine_Lokomotive, nachdem die_Schranke geschlossen_wurde.
205VIPAPS Über_den_Zaun *knickt *ein_Passant, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
232VIPIPS Durch_das_Holzgestell *kracht *ein_Hagelsturm, bevor es völlig in sich zusammenbricht.
220VAPIES Es *drängt *eine_Unruhe durch_die_Menschenmenge, bevor der_Versammlungsplatz geräumt_wird.
213VIPIPS Durch_die_Windböe *wirbelt *eine_Schneeflocke, während sich alle_Leute ins_Warme flüchten.
218VAPIES Es *kriecht *ein_Ölteppich in_die_Kanalisation, während der_Kanaldeckel ausgetauscht_wird.
229VIPAES Es *strauchelt *ein_Leopard über_den_Schneehügel, bevor er auf_vereistem_Boden ins_Rutschen kommt.
231VAPAPS Über_die_Sandbahn *rennt *ein_Igel, während nebenan einige_Schüler Basketball spielen.
233VIPAES Es *bricht *ein_Clown durch_die_Dachluke, nachdem das_Dach neu gedeckt_worden_war.
234VAPAPS Durch_den_Supermarkt *kurvt *eine_Konditorin, solange keine_Hindernisse im_Weg stehen.
213VAPIPS Durch_die_Windböe *jagt *eine_Schneeflocke, während sich alle_Leute ins_Warme flüchten.
211VAPIPS Über_die_Rampe *balanciert *ein_Schaufelbagger, obwohl es in_der_Halle stockdunkel ist.
236VIPIES Es *kullert *ein_Stein über_die_Wiese, während die_Vögel in_den_Bäumen zwitschern.
217VIPIPS In_den_Teich *kippt *ein_Fass, obwohl ein_stabiles_Absperrband rundherum führt.
201VAPAPS Über_das_Parkett *tanzt *eine_Ballerina, während die_Empore des_Saals geputzt_wird.
234VAPAES Es *kurvt *eine_Konditorin durch_den_Supermarkt, solange keine_Hindernisse im_Weg stehen.
220VAPAPS Durch_die_Menschenmenge *drängt *ein_Bäcker, bevor der_Versammlungsplatz geräumt_wird.
224VAPIES Es *wuselt *ein_Snowboard über_den_Gletscher, nachdem sich der_Morgennebel verzogen_hat.
207VIPIES Es *schwebt *eine_Wolke über_den_Wald, solange der_Wind aus_westlicher_Richtung weiter anhält.
231VIPAPS Über_die_Sandbahn *rollt *ein_Igel, während nebenan einige_Schüler Basketball spielen.
222VIPIES Es *schleift *ein_Eimer über_das_Schiff, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
219VIPIES Es *rotiert *ein_Gummiboot durch_die_Stromschnelle, solange genug Wasser hindurchfließt.
215VAPAES Es *pflügt *ein_Buckelwal durch_die_Welle, solange sie vom_Sturm über_das_Meer getrieben_wird.
211VIPIPS Über_die_Rampe *wankt *ein_Schaufelbagger, obwohl es in_der_Halle stockdunkel ist.
202VAPIPS Über_den_See *schwimmt *ein_Frachter, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
219VIPIPS Durch_die_Stromschnelle *rotiert *ein_Gummiboot, solange genug Wasser hindurchfließt.
214VAPIES Es *flitzt *ein_Lichtkegel über_die_Tribüne, bevor das_Sportereignis eröffnet_wird.
232VAPAPS Durch_das_Holzgestell *tobt *ein_Schimpanse, bevor es völlig in sich zusammenbricht.
208VAPAES Es *stampft *ein_Mechaniker über_die_Gleise, nachdem die_Schranke geschlossen_wurde.
236VAPAPS Über_die_Wiese *hüpft *ein_Junge, während die_Vögel in_den_Bäumen zwitschern.
233VIPIPS Durch_die_Dachluke *bricht *ein_Ast, nachdem das_Dach neu gedeckt_worden_war.
215VIPAPS Durch_die_Welle *wogt *ein_Buckelwal, solange sie vom_Sturm über_das_Meer getrieben_wird.
229VAPIES Es *huscht *ein_Schlitten über_den_Schneehügel, bevor er auf_vereistem_Boden ins_Rutschen kommt.
202VAPIES Es *schwimmt *ein_Frachter über_den_See, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
216VIPIPS Über_den_Rasen *kugelt *ein_Fußball, nachdem das_Gras frisch gestutzt_worden_war.
204VAPIPS Durch_die_Manege *schaukelt *ein_Seil, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
202VIPAPS Über_den_See *driftet *ein_Tourist, während das_Ufer von_einem_Spaziergänger beobachtet_wird.
213VIPIES Es *wirbelt *eine_Schneeflocke durch_die_Windböe, während sich alle_Leute ins_Warme flüchten.
223VAPIES Es *schlüpft *eine_Perle aus_dem_Loch, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
227VAPAPS Auf_den_Teller *läuft *ein_Marienkäfer, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
210VAPIPS Durch_die_Redaktion *eilt *eine_Pressemeldung, solange die_Nachricht frisch ist.
207VIPAPS Über_den_Wald *schwebt *ein_Falke, solange der_Wind aus_westlicher_Richtung weiter anhält.
208VAPIES Es *stampft *eine_Lokomotive über_die_Gleise, nachdem die_Schranke geschlossen_wurde.
206VAPIPS Durch_die_Wüste *wandert *eine_Düne, nachdem alle_Pflanzen schon lange ausgestorben_waren.
217VIPAES Es *kippt *ein_Gärtner in_den_Teich, obwohl ein_stabiles_Absperrband rundherum führt.
227VIPAPS Auf_den_Teller *klatscht *ein_Marienkäfer, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
222VAPIPS Über_das_Schiff *poltert *ein_Eimer, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
229VIPAPS Über_den_Schneehügel *strauchelt *ein_Leopard, bevor er auf_vereistem_Boden ins_Rutschen kommt.
207VIPAES Es *schwebt *ein_Falke über_den_Wald, solange der_Wind aus_westlicher_Richtung weiter anhält.
233VAPAPS Durch_die_Dachluke *klettert *ein_Clown, nachdem das_Dach neu gedeckt_worden_war.
215VIPIPS Durch_die_Welle *wogt *ein_Surfbrett, solange sie vom_Sturm über_das_Meer getrieben_wird.
203VIPAES Es *sinkt *ein_Pinguin unter_die_Wasseroberfläche, bevor ein_Hund hinterherspringt.
232VAPIES Es *tobt *ein_Hagelsturm durch_das_Holzgestell, bevor es völlig in sich zusammenbricht.
233VAPAES Es *klettert *ein_Clown durch_die_Dachluke, nachdem das_Dach neu gedeckt_worden_war.
222VAPAPS Über_das_Schiff *poltert *ein_Pirat, bevor die_Mannschaft vom_Schiffskoch zum_Essen gerufen_wird.
220VAPIPS Durch_die_Menschenmenge *drängt *eine_Unruhe, bevor der_Versammlungsplatz geräumt_wird.
224VIPIES Es *kreiselt *ein_Snowboard über_den_Gletscher, nachdem sich der_Morgennebel verzogen_hat.
218VIPIPS In_die_Kanalisation *platscht *ein_Ölteppich, während der_Kanaldeckel ausgetauscht_wird.
221VIPAES Es *knallt *ein_Techniker in_die_Bühnenmitte, obwohl die_Vorstellung schon begonnen_hat.
227VAPAES Es *läuft *ein_Marienkäfer auf_den_Teller, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
218VIPIES Es *platscht *ein_Ölteppich in_die_Kanalisation, während der_Kanaldeckel ausgetauscht_wird.
223VAPIPS Aus_dem_Loch *schlüpft *eine_Perle, nachdem das_Säckchen aus_Leinen aufgeschlitzt_wurde.
225VAPIPS In_die_Grube *zuckelt *ein_Bauwagen, obwohl sie Tag und Nacht bewacht_wird.
230VAPIES Es *biegt *eine_Schubkarre um_die_Ecke, bevor sie auf_dem_Gehweg stehen_bleibt.
219VIPAES Es *rotiert *eine_Wildente durch_die_Stromschnelle, solange genug Wasser hindurchfließt.
210VIPIES Es *zirkuliert *eine_Pressemeldung durch_die_Redaktion, solange die_Nachricht frisch ist.
205VAPIPS Über_den_Zaun *steigt *ein_Strauch, obwohl die_neu angebrachten Holzlatten sehr_hoch sind.
232VAPAES Es *tobt *ein_Schimpanse durch_das_Holzgestell, bevor es völlig in sich zusammenbricht.
204VIPAPS Durch_die_Manege *pendelt *ein_Turner, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
230VIPAPS Um_die_Ecke *schlingert *ein_Briefträger, bevor er auf_dem_Gehweg stehen_bleibt.
226VAPIES Es *krabbelt *eine_Schaumkrone aus_der_Badewanne, bevor das_Badewasser abgelassen_wird.
208VIPIPS Über_die_Gleise *holpert *eine_Lokomotive, nachdem die_Schranke geschlossen_wurde.
217VAPIES Es *rast *ein_Fass in_den_Teich, obwohl ein_stabiles_Absperrband rundherum führt.
206VIPIES Es *treibt *eine_Düne durch_die_Wüste, nachdem alle_Pflanzen schon lange ausgestorben_waren.
230VAPAPS Um_die_Ecke *biegt *ein_Briefträger, bevor er auf_dem_Gehweg stehen_bleibt.
221VAPAES Es *schnellt *ein_Techniker in_die_Bühnenmitte, obwohl die_Vorstellung schon begonnen_hat.
224VAPIPS Über_den_Gletscher *wuselt *ein_Snowboard, nachdem sich der_Morgennebel verzogen_hat.
212VAPIPS Durch_die_Wohnanlage *vagabundiert *eine_Postkarte, solange das von_niemandem beachtet_wird.
206VIPAPS Durch_die_Wüste *treibt *ein_Hirte, nachdem alle_Pflanzen schon lange ausgestorben_waren.
226VAPAES Es *krabbelt *ein_Hotelgast aus_der_Badewanne, bevor das_Badewasser abgelassen_wird.
216VAPAES Es *tänzelt *ein_Stürmer über_den_Rasen, nachdem das_Gras frisch gestutzt_worden_war.
235VAPIES Es *schwirrt *ein_Laserlicht durch_die_Nacht, während der_Mond am_Himmel leuchtet.
217VIPAPS In_den_Teich *kippt *ein_Gärtner, obwohl ein_stabiles_Absperrband rundherum führt.
211VIPAES Es *wankt *ein_Lagerarbeiter über_die_Rampe, obwohl es in_der_Halle stockdunkel ist.
210VIPIPS Durch_die_Redaktion *zirkuliert *eine_Pressemeldung, solange die_Nachricht frisch ist.
227VIPAES Es *klatscht *ein_Marienkäfer auf_den_Teller, nachdem sich der_Vater an_den_Tisch gesetzt_hat.
204VAPAES Es *schaukelt *ein_Turner durch_die_Manege, obwohl die_Raubtiere noch nicht hinausgeführt_wurden.
