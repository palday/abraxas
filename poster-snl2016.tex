% !TEX encoding = UTF-8
% !TEX spellcheck = en_US
\documentclass[final]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{lmodern}
\usepackage{exscale} % fixes some issues with lmodern in math mode
\usepackage[T1]{fontenc}
\usetheme{RJHlandscape}
\usepackage[orientation=landscape,size=a0,scale=1.4]{beamerposter}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,decorations.markings,positioning}
\tikzset{>=stealth}

\usepackage[absolute,overlay]{textpos}
\usepackage[numbers,compress]{natbib}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{array}
%\usepackage{wrapfig}
\usepackage{rotating}

\graphicspath{{figures/}}

\newcolumntype{C}[1]{>{\centering\arraybackslash} m{#1} }
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}
\newcommand{\subblock}[1]{\bigskip\textbf{#1}}

\newcolumntype{$}{>{\global\let\currentrowstyle\relax}}
\newcolumntype{^}{>{\currentrowstyle}}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}%
  #1\ignorespaces
}

\title{The eyes have it: cross-method and cross-linguistic patterns}
\author{P. M. Alday  \and F. Kretzschmar \and S. Luell \and L. Kyriaki \and  M. Schlesewsky \and I. Bornkessel-Schlesewsky}
\institute{University of South Australia}
\footer{}
%\footer{Live code at \url{https://github.com/jona-sassenhagen/phijona---gamma}}
\date{}

\newcommand{\tikzmark}[3][]{\tikz[overlay,remember picture,baseline] \node [anchor=base,#1](#2) {#3};}
%\DeclareMathOperator*{\Pr}{Pr}
% color blind safe colours, see http://www.cookbook-r.com/Graphs/Colors_%28ggplot2%29/#a-colorblind-friendly-palette
\definecolor{lightorange}{HTML}{E69F00}
\definecolor{lightblue}{HTML}{56B4E9}
\definecolor{lightgreen}{HTML}{009E73}
\definecolor{lightyellow}{HTML}{F0E442}
\definecolor{darkblue}{HTML}{0072B2}
\definecolor{darkorange}{HTML}{D55E00}
\definecolor{darkpink}{HTML}{CC79A7}

\begin{document}
\begin{frame}{} 	


\begin{textblock}{48}(1,7)

\begin{block}{Introduction}
Predictive language processing has been investigated in various languages using event-related potentials (ERPs) and eye movements (EM).
Languages differ on which cues predictions are predominantly based \cite{bornkessel-schlesewskyschlesewsky2009a,macwhinneybateskliegl1984a}, but relative cue strength across languages has not been statistically contrasted.
Moreover, the two methods often show different patterning of effects \cite{kretzschmar.schlesewsky.etal:2015jep}, thereby making inferences on cue strength difficult.
\end{block}

\begin{block}{Language and Method Specific Experiments}
We re-analyzed three sentence-reading experiments to capture variance in cue strength as induced by factors \emph{language} (English with dominant word-order cue vs.\ German with dominant case cue) and \emph{method} (ERP vs.\ EM).
There were 37 participants for German ERP, 84 for German eye-tracking (due to an additional between-subjects manipulation) and 16 for English eye-tracking.
\end{block}

\begin{block}{Animacy}
We focused on animacy as a universally available cue. All experiments examined the same interaction of animacy for the subject NP with the semantic verb bias:

\begin{center}
\begin{tabular}{l l l}
\toprule
noun & verb & example \\
\midrule
\textcolor{darkblue}{animate} & \textcolor{darkblue}{animate} & A  \textcolor{darkblue}{ballerina}  \textcolor{darkblue}{danced}  across the stage. \\
\textcolor{darkblue}{animate} & \textcolor{red}{inanimate} & A  \textcolor{darkblue}{ballerina}  \textcolor{red}{floated}  across the stage. \\
\textcolor{red}{inanimate} & \textcolor{darkblue}{animate} & A \textcolor{red}{feather}  \textcolor{darkblue}{danced} across the stage. \\
\textcolor{red}{inanimate} & \textcolor{red}{inanimate} & A \textcolor{red}{feather}  \textcolor{red}{floated} across the stage. \\
\bottomrule
\end{tabular}
\end{center}

While all are possible, the verb ``dance'' is biased towards an animate subject. We expect the first argument to generate a prediction about the animacy (bias) of the second argument, in both SV (English and German) and VS (German only) configurations.
\end{block}

\end{textblock}

\begin{textblock}{23.5}(1,47)
\begin{block}{Results: Eye-tracking}
In both languages, we observed an increase in go-past time in the region following the second argument when there was a mismatch.

\begin{center}
\includegraphics[width=\textwidth]{gopast-de.pdf} \\
\includegraphics[width=\textwidth]{gopast-en.pdf}
\end{center}

\tiny\hfill German data without PP-initial word order.

\vspace{0.48cm}

\end{block}
\end{textblock}

\begin{textblock}{23}(26,47)
\begin{block}{Results: ERP}
In German (RSVP), we observed an N400 on the second argument for inanimate subjects.

\begin{center}
%\includegraphics{erp_PP_prefield.pdf} \\
%\includegraphics{erp_es_prefield.pdf}
\includegraphics{erp_animate_verb.pdf} \\
\includegraphics{erp_inanimate_verb.pdf}
\end{center}

\tiny\hfill VS word order.

\end{block}


\end{textblock}

\begin{textblock}{45}(50.5,7)
\begin{block}{Meta-Experiments}

These experiments form a meta-experiment in two ways:  the eye-tracking data form a between-subjects design for the \emph{language} factor, while the German data form together a between-subjects design for the \emph{method} factor.

\end{block}

\begin{block}{Between languages}
\begin{tabular}{c p{1cm} c}
\begin{minipage}{0.55\textwidth}
\input{eyes-across-languages.tex}

\bigskip

\hfill \tiny Type-II tests with denominator degrees of freedom computed via the Satterthwaite approximation.

\end{minipage}
%
& &
%
\begin{minipage}{0.35\textwidth}
The combined eye-tracking data can be used to examine the variation in animacy cue-strength across languages.
Using mixed-effects models, we found no main effect nor any interactions for language, indicating that the animacy effect was equally strong across both languages.
%\includegraphics[width=\textwidth]{effect-crosslanguage.pdf}
\end{minipage}

\end{tabular}

\end{block}

\begin{block}{Between methods} 
We combined eye-tracking and ERP data by aggregating one measure across participants (who differed between methods) to provide a numeric predictor for the other with mixed-effects models. For eye-tracking, we used go-past time on the prepositional phrase. For EEG, we used the mean amplitude in the N400 time window (300--500ms post stimulus).

\vspace{0.9cm}

\begin{tabular}{l p{0cm} r}
\begin{minipage}{0.48\textwidth}
{\hfill Go-Past Time \hfill}

\bigskip

\input{eyes-from-eeg.tex}


\end{minipage}
%
& &
%
\begin{minipage}{0.48\textwidth}
{\hfill N400 \hfill}

\bigskip
\input{eeg-from-eyes.tex}

\end{minipage}

\end{tabular}

\vspace{0.9cm}

Interestingly, the models based on the aggregate data provide a surprisingly good fit, approaching or even exceeding the parametric fits based on the factorial experimental manipulation.

\vspace{0.9cm}

\begin{tabular}{l p{1cm} r}
\begin{minipage}{0.48\textwidth}
{\hfill Go-Past Time \hfill}

\bigskip

\input{eeg-from-eyes-model.tex}

\end{minipage}
%
& &
%
\begin{minipage}{0.48\textwidth}
{\hfill N400 \hfill}

\bigskip

\input{eyes-from-eeg-model.tex}

\end{minipage}

\end{tabular}

\vspace{1.5cm}

As individual predictors, the EEG data also seem to better predict the eye-tracking data than the other way around (the t-ratio of estimate-to-error is larger).

\end{block}

\end{textblock}

\begin{textblock}{21}(97,7)

\begin{block}{Conclusion}

Animacy remains important to predictive processing even in languages where sentence processing is more strictly based on linear word-order.

\bigskip

Despite the lack of alternative interpretations in English, a less than ideal subject or Actor argument still elicits as strong a prediction error in English as in German.

\bigskip

We have demonstrated a quantitative relationship between the eyes and the brain in which N400 activity \emph{from other participants} better predicts go-past time than the parametric experimental manipulation. 


\bigskip

The complex relationships among particular ERP components and EM measures reveal a complex hierarchy of predictive processes.

\end{block}

\begin{block}{Literature}
\tiny
\bibliographystyle{poster}
\bibliography{snl2016.bib}
\vspace{0.25cm}
\end{block}

\begin{block}{Get a copy!}
\centering
\includegraphics[width=0.6\textwidth]{poster-snl2016-url.eps}

doi: 10.6084/m9.figshare.3507347

\begin{tabular}{c c c}
\includegraphics[width=.55\linewidth]{logo_unisa_RGB-blue.png} &   \includegraphics[width=.4\linewidth]{cnl-color.eps} & \\
\end{tabular}

\includegraphics[width=.9\linewidth]{logo_mainz_new.eps}

\vspace{0.12cm}
\end{block}


\end{textblock}

\end{frame}
\end{document}
