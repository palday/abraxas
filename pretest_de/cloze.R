#installation of lmerOut on my local Mac requires different install_bitbucket command
#if(!require(devtools)){
#   install.packages("devtools")
#   library("devtools")
# }
#install_bitbucket("lmerOut", username="palday")
#use this on linux server: install_bitbucket("palday/lmerOut")

library(lme4)
library(lmerOut)
library(ggplot2)
library(grid)
#library(beepr)
#library(plyr)
library(effects)
library(lsmeans)
#library(nloptr)
#library(nlme)
library(ez)
library(car)
library(pastecs)

# read data
cloze = read.table("FB02_ClozeTest_Input.txt",header=T)

#exclude items that were not used in the eye tracking experiment
cloze2 = cloze[cloze$set != "103",]
cloze2 = cloze2[cloze2$set != "108",]
cloze2 = cloze2[cloze2$set != "110",]
cloze2 = cloze2[cloze2$set != "111",]
cloze2 = cloze2[cloze2$set != "112",]
cloze2 = cloze2[cloze2$set != "113",]
cloze2 = cloze2[cloze2$set != "122",]
cloze2 = cloze2[cloze2$set != "125",]
cloze2 = cloze2[cloze2$set != "126",]
cloze2 = cloze2[cloze2$set != "137",]
cloze2 = cloze2[cloze2$set != "142",]
cloze2 = cloze2[cloze2$set != "147",]


sink("FB02_descriptive_stats_pretest_clozeT.txt")

print("## 48 sets (= pretest for all sets) ##",quote=F)
print("",quote=F)
print("## Descriptive stats - Cloze Task ##",quote=F)
print("",quote=F)
print("## stat.desc ##",quote=F)
print(by(cloze[,c("value")], cloze$cond, stat.desc, basic = F, norm = T), quote=F)
print("",quote=F)
print("## By participant (ezStats) ##",quote=F)
print("",quote=F)
cloze$set = as.factor(cloze$set)
print(ezStats(
  data = cloze,
  dv =  value,
  wid = VP,
  within = .(cond)
  )
)
print("",quote=F)
print("## By item (ezStats) ##",quote=F)
print("",quote=F)
print(ezStats(
  data = cloze,
  dv =  value,
  wid = set,
  within = .(cond)
  )
)
print("",quote=F)
print("",quote=F)

print("## 36 sets (= pretest sets selected for ET-study) ##",quote=F)
print("",quote=F)
print("## Descriptive stats - cloze Task ##",quote=F)
print("",quote=F)
print("## stat.desc ##",quote=F)
print(by(cloze2[,c("value")], cloze2$cond, stat.desc, basic = F, norm = T), quote=F)
print("",quote=F)
print("## By participant (MVAN= animate motion verb)##",quote=F)
print("",quote=F)
cloze$set = as.factor(cloze$set)
print(ezStats(
  data = cloze2,
  dv =  value,
  wid = VP,
  within = .(cond)
  )
)
print("",quote=F)
print("## By item ##",quote=F)
print("",quote=F)
print(ezStats(
  data = cloze2,
  dv =  value,
  wid = set,
  within = .(cond)
  )
)
print("",quote=F)
print("",quote=F)

print("## glmer test: Cloze Task (48 sets)##",quote=F)

#recode 1(anim) and 2(inanim) with 0 and 1, respectively
cloze$value[cloze$value == 1] <- 0
cloze$value[cloze$value == 2] <- 1

cloze$cond <- factor(cloze$cond, levels=c("MVAN", "MVIN"), labels=c("animate", "inanimate"))
options(contrasts=c("contr.Sum","contr.helmert"))

#run a logistic regression model 
cloze.max <- glmer(value ~ 1 + cond + (1+cond|VP) + (1+cond|set), family=binomial, data=cloze, glmerControl(optimizer ="bobyqa", calc.derivs=FALSE))
# by-item random slope for cond has corr -1 in RE structure

print(Anova(cloze.max))
print(summary(cloze.max))

print("##########################################################,quote=F")
#print("## glmer test: Cloze Task (36 sets)/ results identical to glmer with 48 item sets##",quote=F)

#recode 1(anim) and 2(inanim) with 0 and 1, respectively
#cloze2$value[cloze2$value == 1] <- 0
#cloze2$value[cloze2$value == 2] <- 1

#cloze2$cond <- factor(cloze2$cond, levels=c("MVAN", "MVIN"), labels=c("animate", "inanimate"))
#options(contrasts=c("contr.Sum","contr.helmert"))

#run a logistic regression model 
#cloze2.max <- glmer(value ~ 1 + cond + (1+cond|VP) + (1+cond|set), family=binomial, data=cloze2)
# by-item random slope for cond has corr -1 in RE structure

#print(Anova(cloze2.max))
#print(summary(cloze2.max))

sink()
sink("FB03_descriptive_stats_pretest_semanticfit.txt")
print("##########################################################,quote=F")
print("## Pretest 2: semantic fit##",quote=F)

semfit <- read.table("FB03_SemFit_24_Input.txt", header=T)

semfit$set = as.factor(semfit$set)

#exclude items that were not used in the eye tracking experiment
semfit2 = semfit[semfit$set != "103",]
semfit2 = semfit2[semfit2$set != "108",]
semfit2 = semfit2[semfit2$set != "110",]
semfit2 = semfit2[semfit2$set != "111",]
semfit2 = semfit2[semfit2$set != "112",]
semfit2 = semfit2[semfit2$set != "113",]
semfit2 = semfit2[semfit2$set != "122",]
semfit2 = semfit2[semfit2$set != "125",]
semfit2 = semfit2[semfit2$set != "126",]
semfit2 = semfit2[semfit2$set != "137",]
semfit2 = semfit2[semfit2$set != "142",]
semfit2 = semfit2[semfit2$set != "147",]

print("## 48 sets (= pretest all sets) ##",quote=F)
print("",quote=F)
print("## Descriptive stats - Semantic fit Task ##",quote=F)
print("",quote=F)
print("## stat.desc Bedingung1 = Verbtyp (MVAN_animate motion verb; Bedingung2: Animacy (PCAN_animate actor)) ##",quote=F)
print(by(semfit[,c("Antwort")], list(semfit$Bedingung1, semfit$Bedingung2), stat.desc, basic = F, norm = T), quote=F)
print("",quote=F)
print("## By participant (ezStats) Bedingung1 = Verbtyp (MVAN_animate motion verb; Bedingung2: Animacy (PCAN_animate actor))##",quote=F)
print("",quote=F)
print(ezStats(
  data = semfit,
  dv =  Antwort,
  wid = VP,
  within = .(Bedingung1, Bedingung2)
  )
)
print("",quote=F)
print("## By item (ezStats) ##",quote=F)
print("",quote=F)
print(ezStats(
  data = semfit,
  dv =  Antwort,
  wid = set,
  within = .(Bedingung1, Bedingung2)
  )
)
print("",quote=F)
print("",quote=F)

print("## 36 sets (= pretest sets selected for ET-study) ##",quote=F)
print("",quote=F)
print("## stat.desc Bedingung1 = Verbtyp (MVAN_animate motion verb; Bedingung2: Animacy (PCAN_animate actor)) ##",quote=F)
print(by(semfit[,c("Antwort")], list(semfit$Bedingung1, semfit$Bedingung2), stat.desc, basic = F, norm = T), quote=F)
print("## By participant (ezStats) ##",quote=F)
print("",quote=F)
print(ezStats(
  data = semfit2,
  dv =  Antwort,
  wid = VP,
  within = .(Bedingung1, Bedingung2)
  )
)
print("",quote=F)
print("## By item ##",quote=F)
print("",quote=F)
print(ezStats(
  data = semfit2,
  dv =  Antwort,
  wid = set,
  within = .(Bedingung1, Bedingung2)
  )
)
print("",quote=F)
print("",quote=F)

print("## statistical test: SemFit Task (48 sets)##",quote=F)

semfit$verb <- factor(semfit$Bedingung1, levels=c("MVAN", "MVIN"), labels=c("animate", "inanimate"))
semfit$animacy <- factor(semfit$Bedingung2, levels=c("PCAN", "PCIN"), labels=c("animate", "inanimate"))
options(contrasts=c("contr.Sum","contr.helmert"))

#or use lmer with scaled response (z-transformation), log transformation prior to scaling is optional
#(Schuetze & Sprouse 2013)
semfit$Antwort.scale <-scale(semfit$Antwort)
semfit.max <- lmer(Antwort.scale ~ 1+ verb*animacy + (1+verb*animacy|VP) + (1+verb*animacy|set), REML=FALSE, data=semfit, lmerControl(optimizer="bobyqa", calc.derivs=FALSE)) 
print(Anova(semfit.max))
print(summary(semfit.max))

print("lsmeans pairwise comparisons")
semfit.lsm <- lsmeans(semfit.max,~verb*animacy)
print(pairs(semfit.lsm))

######ALTERNATIVE MODELS#####
#run a mixed effects ordinal logistic regression model 
#use MCMCglmm() as glmer only fits binary data (if glmer is used, resonse should be scaled)
#library(MCMCglmm)
#model.semfit <- MCMCglmm(Antwort ~ animacy*verb, random=~VP+set, data=semfit2, verbose=F)
#print(summary(model.semfit))



#ordered probit regression for ordinal data
#fit cumulative linked mixed model, i.e. regression mixed effects model for ordinal data

#library(ucminf)
#library(ordinal)
#Note that you need the latest version of Ordinal package - 2013.9-30
#http://cran.r-project.org/web/packages/ordinal/index.html
#It is available in the latest version of R - R 3.0.2

#order the data (also if they are coded numerically; response needs to be a factor for the model to run successfully)
#semfit$Score=ordered(semfit$Antwort, levels=c("1","2","3","4"))
#check ordering
#head(semfit$Score)

#run the model; not possible to include random slopes

#fm1 <- clmm(Score ~ verb*animacy + (1|VP) + (1|set), data=semfit, Hess=TRUE)
#summary(fm1)

sink()
sink("FB01_descriptive_stats_pretest_plausibility.txt")
print("##########################################################,quote=F")
print("## Pretest 3: plausibility##",quote=F)

plaus <- read.table("FB01_PlausInputR.txt", header=T)


plaus2 = plaus[plaus$set != "103",]
plaus2 = plaus2[plaus2$set != "108",]
plaus2 = plaus2[plaus2$set != "110",]
plaus2 = plaus2[plaus2$set != "111",]
plaus2 = plaus2[plaus2$set != "112",]
plaus2 = plaus2[plaus2$set != "113",]
plaus2 = plaus2[plaus2$set != "122",]
plaus2 = plaus2[plaus2$set != "125",]
plaus2 = plaus2[plaus2$set != "126",]
plaus2 = plaus2[plaus2$set != "137",]
plaus2 = plaus2[plaus2$set != "142",]
plaus2 = plaus2[plaus2$set != "147",]

print("## 48 sets (= pretest all sets) ##",quote=F)
print("",quote=F)
print("## Descriptive stats - Plausibility Task ##",quote=F)
print("",quote=F)
print("## stat.desc Bedingung1 = Verbtyp (MVAN_animate motion verb; Bedingung2: Animacy (PCAN_animate actor)) ##",quote=F)
print(by(plaus[,c("Antwort")], list(plaus$Bedingung1, plaus$Bedingung2), stat.desc, basic = F, norm = T))
print("",quote=F)
print("## By participant (ezStats) Bedingung1 = Verbtyp (MVAN_animate motion verb; Bedingung2: Animacy (PCAN_animate actor)) ##",quote=F)
print("",quote=F)
plaus$set = as.factor(plaus$set)

print(ezStats(
  data = plaus,
  dv =  Antwort,
  wid = VP,
  within = .(Bedingung1, Bedingung2)
  )
)
print("",quote=F)
print("## By item (ezStats) ##",quote=F)
print("",quote=F)
print(ezStats(
  data = plaus,
  dv =  Antwort,
  wid = set,
  within = .(Bedingung1, Bedingung2)
  )
)
  
print("",quote=F)
print("",quote=F)

print("## 36 sets (= pretest sets selected for ET-study) ##",quote=F)
print("",quote=F)
print("## stat.desc Bedingung1 = Verbtyp (MVAN_animate motion verb; Bedingung2: Animacy (PCAN_animate actor)) ##",quote=F)
print(by(plaus[,c("Antwort")], list(plaus$Bedingung1, plaus$Bedingung2), stat.desc, basic = F, norm = T))
print("",quote=F)
print("## By participant ##",quote=F)
print("",quote=F)
print(ezStats(
  data = plaus2,
  dv =  Antwort,
  wid = VP,
  within = .(Bedingung1, Bedingung2)
  )
)
print("",quote=F)
print("## By item ##",quote=F)
print("",quote=F)
print(ezStats(
  data = plaus2,
  dv =  Antwort,
  wid = set,
  within = .(Bedingung1, Bedingung2)
  )
)
print("",quote=F)
print("",quote=F)

print("## statistical test: Plausibility Task (48 sets)##",quote=F)

plaus$verb <- factor(plaus$Bedingung1, levels=c("MVAN", "MVIN"), labels=c("animate", "inanimate"))
plaus$animacy <- factor(plaus$Bedingung2, levels=c("PCAN", "PCIN"), labels=c("animate", "inanimate"))
options(contrasts=c("contr.Sum","contr.helmert"))

#or use lmer with scaled response (z-transformation), log transformation prior to scaling is optional
#(Schuetze & Sprouse 2013)
plaus$Antwort.scale <-scale(plaus$Antwort)
plaus.max <- lmer(Antwort.scale ~ 1+ verb*animacy + (1+verb*animacy|VP) + (1+verb*animacy|set), REML=FALSE, data=plaus, lmerControl(optimizer="bobyqa", calc.derivs=FALSE)) 
print(Anova(plaus.max))
print(summary(plaus.max))

print("lsmeans - pairwise comparisons")
plaus.lsm <- lsmeans(plaus.max, ~verb*animacy)
print(pairs(plaus.lsm))

#effect plots for each pretest

#cloze pretest
e.cloze <- Effect(c("cond"),cloze.max,KR=F)
e.cloze.data <- as.data.frame(e.cloze)
e.cloze.data$cond <- factor(e.cloze.data$cond, levels=c("animate","inanimate"), labels=c("animate", "inanimate"))

#semfit pretest
e.semfit <- Effect(c("verb","animacy"),semfit.max,KR=F)
e.semfit.data <- as.data.frame(e.semfit)
e.semfit.data$verb <- factor(e.semfit.data$verb, levels=c("animate","inanimate"), labels=c("animate", "inanimate"))
e.semfit.data$animacy <- factor(e.semfit.data$animacy, levels=c("animate","inanimate"), labels=c("animate", "inanimate"))

#plausibility pretest
e.plaus <- Effect(c("verb","animacy"),plaus.max,KR=F)
e.plaus.data <- as.data.frame(e.plaus)
e.plaus.data$verb <- factor(e.plaus.data$verb, levels=c("animate","inanimate"), labels=c("animate", "inanimate"))
e.plaus.data$animacy <- factor(e.plaus.data$animacy, levels=c("animate","inanimate"), labels=c("animate", "inanimate"))

#plot cloze task
clozeplot <- ggplot(e.cloze.data,aes(x=cond,y=fit,color=cond)) + 
  geom_point(shape=17, size=5) + 
  geom_errorbar(aes(ymin=lower,ymax=upper),size =1, width=0.2) + 
  ylab("Fitted Mean cloze probability") + 
  xlab("") + 
  scale_color_manual(values = c("steelblue3", "indianred3"), name="Animacy requirement of the verb") +  
  scale_x_discrete(breaks="white") +
  theme_light() +
  #theme(legend.justification=c(0.05,0.9), legend.position=c(0,1)) + 
  #theme(legend.background = element_rect(fill="gray95", size=.5, linetype="solid")) +
  ggtitle(paste0("Animacy cloze task:(0 = animate actor,\n1= inanimate actor) as modelled"))

ggsave("clozeplot.ps", height=4,width=6.5)

#plots semantic fit task

semfit_verbx <- ggplot(e.semfit.data,aes(x=verb,y=fit,color=animacy)) + 
  geom_point(shape=17, size=5) + 
  geom_errorbar(aes(ymin=lower,ymax=upper),size =1, width=0.2) + 
  ylab("Fitted Mean Rating (1(best)-4(worst))") + 
  xlab("Verb type") + 
  scale_color_manual(values = c("steelblue3", "indianred3"), name="Actor animacy") +  
  theme_light() +
  #theme(legend.justification=c(0.05,0.9), legend.position=c(0,1)) + 
  #theme(legend.background = element_rect(fill="gray95", size=.5, linetype="solid")) +
  ggtitle(paste0("Interaction of Verb type and Actor animacy as modelled"))

ggsave("semfit_verbx.ps", height=4,width=6.5)

semfit_nounx <- ggplot(e.semfit.data,aes(x=animacy,y=fit,color=verb)) + 
  geom_point(shape=17, size=5) + 
  geom_errorbar(aes(ymin=lower,ymax=upper),size =1, width=0.2) + 
  ylab("Fitted Mean Rating (1(best)-4(worst))") + 
  xlab("Actor animacy") + 
  scale_color_manual(values = c("steelblue3", "indianred3"), name="Verb type") +  
  theme_light() +
  #theme(legend.justification=c(0.05,0.9), legend.position=c(0,1)) + 
  #theme(legend.background = element_rect(fill="gray95", size=.5, linetype="solid")) +
  ggtitle(paste0("Interaction of Verb type and Actor animacy as modelled"))

ggsave("semfit_nounx.ps", height=4,width=6.5)

#plots plausibility task

plaus_verbx <- ggplot(e.plaus.data,aes(x=verb,y=fit,color=animacy)) + 
  geom_point(shape=17, size=5) + 
  geom_errorbar(aes(ymin=lower,ymax=upper),size =1, width=0.2) + 
  ylab("Fitted Mean Rating (1(best)-4(worst))") + 
  xlab("Verb type") + 
  scale_color_manual(values = c("steelblue3", "indianred3"), name="Actor animacy") +  
  theme_light() +
  #theme(legend.justification=c(0.05,0.9), legend.position=c(0,1)) + 
  #theme(legend.background = element_rect(fill="gray95", size=.5, linetype="solid")) +
  ggtitle(paste0("Interaction of Verb type and Actor animacy as modelled"))

ggsave("plaus_verbx.ps", height=4,width=6.5)

plaus_nounx <- ggplot(e.plaus.data,aes(x=animacy,y=fit,color=verb)) + 
  geom_point(shape=17, size=5) + 
  geom_errorbar(aes(ymin=lower,ymax=upper),size =1, width=0.2) + 
  ylab("Fitted Mean Rating (1(best)-4(worst))") + 
  xlab("Actor animacy") + 
  scale_color_manual(values = c("steelblue3", "indianred3"), name="Verb type") +  
  theme_light() +
  #theme(legend.justification=c(0.05,0.9), legend.position=c(0,1)) + 
  #theme(legend.background = element_rect(fill="gray95", size=.5, linetype="solid")) +
  ggtitle(paste0("Interaction of Verb type and Actor animacy as modelled"))

ggsave("plaus_nounx.ps", height=4,width=6.5)

sink()