#read in data
sink("item_median_pretests.txt")
#gp <- read.table("gp.txt", sep=" ", header=T)
sf <- read.table("FB03_SemFit_24_Input.txt", header=T)
pl <- read.table("FB01_PlausInputR.txt", header=T)
cl <- read.table("FB02_ClozeTest_Input.txt",header=T)

cl$Bedingung2 <- rep("x",length(cl$VP))
cl1 <- data.frame(VP=cl$VP, set=cl$set, Bedingung1=cl$cond, Bedingung2=cl$Bedingung2, Antwort=cl$value)

sf$test <- "sf"
pl$test <- "pl"
cl1$test <- "cl"
pretest <- rbind(sf,pl,cl1)

#recode item numbers for 36 sets as used in eye tracking experiment 
pretest$set[pretest$set == 101] <- 1
pretest$set[pretest$set == 102] <- 2
pretest$set[pretest$set == 104] <- 3
pretest$set[pretest$set == 105] <- 4
pretest$set[pretest$set == 106] <- 5
pretest$set[pretest$set == 107] <- 6
pretest$set[pretest$set == 109] <- 7
pretest$set[pretest$set == 114] <- 8
pretest$set[pretest$set == 115] <- 9
pretest$set[pretest$set == 116] <- 10
pretest$set[pretest$set == 117] <- 11
pretest$set[pretest$set == 118] <- 12
pretest$set[pretest$set == 119] <- 13
pretest$set[pretest$set == 120] <- 14
pretest$set[pretest$set == 121] <- 15
pretest$set[pretest$set == 123] <- 16
pretest$set[pretest$set == 124] <- 17
pretest$set[pretest$set == 127] <- 18
pretest$set[pretest$set == 128] <- 19
pretest$set[pretest$set == 129] <- 20
pretest$set[pretest$set == 130] <- 21
pretest$set[pretest$set == 131] <- 22
pretest$set[pretest$set == 132] <- 23
pretest$set[pretest$set == 133] <- 24
pretest$set[pretest$set == 134] <- 25
pretest$set[pretest$set == 135] <- 26
pretest$set[pretest$set == 136] <- 27
pretest$set[pretest$set == 138] <- 28
pretest$set[pretest$set == 139] <- 29
pretest$set[pretest$set == 140] <- 30
pretest$set[pretest$set == 141] <- 31
pretest$set[pretest$set == 143] <- 32
pretest$set[pretest$set == 144] <- 33
pretest$set[pretest$set == 145] <- 34
pretest$set[pretest$set == 146] <- 35
pretest$set[pretest$set == 148] <- 36

#exclude the other 12 item sets
pretest$set[pretest$set == 103] <- NA
pretest$set[pretest$set == 108] <- NA
pretest$set[pretest$set == 110] <- NA
pretest$set[pretest$set == 111] <- NA
pretest$set[pretest$set == 112] <- NA
pretest$set[pretest$set == 113] <- NA
pretest$set[pretest$set == 122] <- NA
pretest$set[pretest$set == 125] <- NA
pretest$set[pretest$set == 126] <- NA
pretest$set[pretest$set == 137] <- NA
pretest$set[pretest$set == 142] <- NA
pretest$set[pretest$set == 147] <- NA

#recode factor levels
pretests <- subset(pretest, !is.na(set))
pretests$Verbtyp <- factor(pretests$Bedingung1
                       ,levels=c("MVIN", "MVAN")
                       ,labels=c("inanimate","animate"))
pretests$Animacy <- factor(pretests$Bedingung2
                       ,levels=c("PCIN", "PCAN", "x")
                       ,labels=c("inanimate","animate", "x"))

# split pretests                       
cl <- subset(pretests, test=="cl")
sf <- subset(pretests, test=="sf")
pl <- subset(pretests, test=="pl")
names(cl) <- c("subj", "item", "verb", "dummy","cl_result", "test", "Verbtype", "Animacy")
names(sf) <- c("subj", "item", "verb", "animacy","sf_result", "test", "Verbtype", "Animacy")
names(pl) <- c("subj", "item", "verb", "animacy","pl_result", "test", "Verbtype", "Animacy")

#cloze task item median
print("cloze task (1=animate filled in, 2= inanimate filled in): item median", quote=F)
data_clitems <- aggregate(cl$cl_result, list(verb=cl$Verbtype, item=cl$item), median, na.rm=T)
names(data_clitems) <- c("verb","item","clmedian")
print(data_clitems)
print("", quote=F)

#semantic fit task item median
print("semantic fit task (1= very high fit, 4= very low fit): item median", quote=F)
data_sfitems <- aggregate(sf$sf_result, list(verb=sf$Verbtype, animacy=sf$Animacy, item=sf$item), median, na.rm=T)
names(data_sfitems) <- c("verb","animacy","item","sfmedian")
print(data_sfitems)
print("", quote=F)

#plausibility task item median
print("plausibility task (1=very plausible, 4= very implausible): item median", quote=F)
data_plitems <- aggregate(pl$pl_result, list(verb=pl$Verbtype, animacy=pl$Animacy, item=pl$item), median, na.rm=T)
names(data_plitems) <- c("verb","animacy","item","plmedian")
print(data_plitems)
print("", quote=F)

#create new vectors for et data that contains item median per pretest
cloze <- rep(0,length(gp$item))
semfit <- rep(0,length(gp$item))
plaus <- rep(0,length(gp$item))

###mit awk-Befehlen vorgefertigt; "" muss noch bei Verbtyp 
cloze[gp$item ==1 & gp$Verbtyp =="animate"] <- 1.000000
cloze[gp$item ==1 & gp$Verbtyp =="inanimate"] <- 1.375000
cloze[gp$item ==2 & gp$Verbtyp =="animate"] <-
cloze[gp$item ==2 & gp$Verbtyp =="inanimate"] <-

gp_new <- cbind(gp, cloze, semfit, plaus)

sink()














