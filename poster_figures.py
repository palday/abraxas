# -*- coding: utf-8 -*-

# run graphics generation even on a headless setup
# http://stackoverflow.com/a/4706614/2022326
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!

import pylab
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import mne
import numpy as np
import os.path
import seaborn as sns
from mne.viz import plot_evoked_topo

mne.set_log_level('WARNING')
layout = mne.channels.read_layout("EEG1005.lay",scale=True)
layout.pos[:,2] = layout.pos[:,2] * 4
layout.pos[:,3] = layout.pos[:,3] * 5
montage = mne.channels.read_montage(kind="standard_1020")

try:
    import cPickle as pickle
except ImportError:
    import pickle

with open("cache.pickle","rb") as pfile:
    evoked = pickle.load(pfile)
    evoked_by_cond = pickle.load(pfile)
    grand_average = pickle.load(pfile)

# event trigger and conditions
event_id = {'iien': 182,
            'iiev': 181,
            'iaen': 172,
            'iaev': 171,
            'aien': 162,
            'aiev': 161,
            'aaen': 152,
            'aaev': 151,
            'iipn': 142,
            'iipv': 141,
            'iapn': 132,
            'iapv': 131,
            'aipn': 122,
            'aipv': 121,
            'aapn': 112,
            'aapv': 111}

for e in evoked.keys():
    if sum(evoked[e][c].nave for c in evoked[e]) < 60:
        del evoked[e]

print len(evoked.keys())
evoked_by_cond = {e:[evoked[s][e] for s in evoked] for e in event_id if "n" in e}
grand_average = {cond:mne.grand_average(evoked_by_cond[cond]) for cond in evoked_by_cond}

#blue = (0.25646972,  0.48715914,  0.6570037)
#red  = (0.85453713,  0.22957019,  0.2762321)

blue = (  0 / 255.0, 114 / 255.0, 178 / 255.0)
red =  (237 / 255.0,  27 / 255.0,  35 / 255.0)

time = np.linspace(-200,1200,num=701)

aan = mne.grand_average( [grand_average['aa'+pf+'n'].copy() for pf in ('e','p')] )
ain = mne.grand_average( [grand_average['ai'+pf+'n'].copy() for pf in ('e','p')] )
ian = mne.grand_average( [grand_average['ia'+pf+'n'].copy() for pf in ('e','p')] )
iin = mne.grand_average( [grand_average['ii'+pf+'n'].copy() for pf in ('e','p')] )

ylim = 4,-4    
    
aan = aan.pick_channels(['Cz'])
ain = ain.pick_channels(['Cz'])
ian = ian.pick_channels(['Cz'])
iin = iin.pick_channels(['Cz'])

# animate noun
plt.plot(time,aan.data[0,:]*1e6,color=blue,label="anim verb - anim noun")
plt.plot(time,ian.data[0,:]*1e6,color=red, label="inanim verb - anim noun")

plt.xlim(time[0],time[-1])
plt.hlines(0,time[0],time[-1])
plt.xlabel(u"Time (ms)")

ylim = 5,-5
plt.ylim(*ylim)
plt.vlines(0,*ylim)
plt.ylabel(u"µV")

plt.title("Animate noun at electrode Cz")
plt.legend()
plt.savefig(os.path.join("figures","erp_animate_noun.pdf".format('es' if pf == 'e' else 'PP')), bbox_inches='tight')
#plt.show()
plt.close()

# inanimate noun
plt.plot(time,ain.data[0,:]*1e6,color=blue,label="anim verb - inanim noun")
plt.plot(time,iin.data[0,:]*1e6,color=red, label="inanim -verb - inanim noun")

plt.xlim(time[0],time[-1])
plt.hlines(0,time[0],time[-1])
plt.xlabel(u"Time (ms)")

ylim = 5,-5
plt.ylim(*ylim)
plt.vlines(0,*ylim)
plt.ylabel(u"µV")

plt.title("Inanimate noun at electrode Cz")
plt.legend()
plt.savefig(os.path.join("figures","erp_inanimate_noun.pdf".format('es' if pf == 'e' else 'PP')), bbox_inches='tight')
#plt.show()
plt.close()

# animate verb
plt.plot(time,aan.data[0,:]*1e6,color=blue,label="animate noun")
plt.plot(time,ain.data[0,:]*1e6,color=red, label="inanimate noun")

plt.xlim(time[0],time[-1])
plt.hlines(0,time[0],time[-1])
plt.xlabel(u"Time (ms)")

ylim = 5,-5
plt.ylim(*ylim)
plt.vlines(0,*ylim)
plt.ylabel(u"µV")

plt.title("Cz at the noun (animate verb)")
plt.legend()
plt.savefig(os.path.join("figures","erp_animate_verb.pdf".format('es' if pf == 'e' else 'PP')), bbox_inches='tight')
#plt.show()
plt.close()

# inanimate verb
plt.plot(time,ian.data[0,:]*1e6,color=blue,label="animate noun")
plt.plot(time,iin.data[0,:]*1e6,color=red, label="inanimate noun")

plt.xlim(time[0],time[-1])
plt.hlines(0,time[0],time[-1])
plt.xlabel(u"Time (ms)")

ylim = 5,-5
plt.ylim(*ylim)
plt.vlines(0,*ylim)
plt.ylabel(u"µV")

plt.title("Cz at the noun (inanimate verb)")
plt.legend()
plt.savefig(os.path.join("figures","erp_inanimate_verb.pdf".format('es' if pf == 'e' else 'PP')), bbox_inches='tight')
#plt.show()
plt.close()

